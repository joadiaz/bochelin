package com.bochelin.repositories;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.FlavourDAO;
import com.bochelin.domain.Flavour;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("flavourRepository")
public class FlavourRepository extends AbstractDAO<Integer, Flavour> implements FlavourDAO {

    public void saveEntity(Flavour flavour) {
        persist(flavour);
    }

    public void deleteEntity(Flavour flavour) { delete(flavour);}

    public List<Flavour> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public Flavour findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Flavour) criteria.uniqueResult();
    }

    public List<Flavour> search(String filter){
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.like("name", filter, MatchMode.START).ignoreCase());
        return criteria.list();
    }

    public List<Flavour> searchByCategory(int categoryId, String filter){
        Criteria criteria = createEntityCriteria();
        Criterion nameLike = Restrictions.like("name", filter, MatchMode.ANYWHERE).ignoreCase();
        Criterion descriptionLike = Restrictions.like("description", filter, MatchMode.ANYWHERE).ignoreCase();
        Criterion category = Restrictions.eq("flavourCategory.id", categoryId);
        criteria.add(Restrictions.or(nameLike, descriptionLike));
        criteria.add(category);
        return criteria.list();
    }

}
