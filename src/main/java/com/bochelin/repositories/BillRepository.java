package com.bochelin.repositories;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.BillsDAO;
import com.bochelin.domain.Bill;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository("billsRepository")
public class BillRepository extends AbstractDAO<Integer, Bill> implements BillsDAO{

    public void saveEntity(Bill bill) {
        persist(bill);
    }

    public void deleteEntity(Bill bill) {
        persist(bill);
    }

    public List<Bill> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public Bill findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Bill) criteria.uniqueResult();
    }

    public Bill getLastAddedBill(){
        Criteria criteria = createEntityCriteria();
        criteria.addOrder(Order.desc("id")).setMaxResults(1);
        return (Bill) criteria.uniqueResult();
    }

    public List<Bill> getBillsBetweenDates(Date fromDate, Date toDate) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.between("billDate", fromDate, toDate));
        criteria.addOrder(Order.desc("billDate"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

}
