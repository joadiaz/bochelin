package com.bochelin.repositories;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.UserDAO;
import com.bochelin.domain.User;

import java.util.List;

@Repository("userRepository")
public class UserRepository extends AbstractDAO<Integer, User> implements UserDAO {

    private static final Logger logger = Logger.getLogger(UserRepository.class);

    public User findByUsername(String username) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("username", username));
        return (User) criteria.uniqueResult();
    }

    public void saveUser(User user) {
        logger.debug(user.getUsername());
        persist(user);
    }

    public void deleteUser(User user) {
        delete(user);
    }

    public List<User> findAllUsers() {
        Criteria criteria = createEntityCriteria();
        return (List<User>) criteria.list();
    }

    public User findByEmail(String email){
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("email", email));
        return (User) criteria.uniqueResult();
    }

}
