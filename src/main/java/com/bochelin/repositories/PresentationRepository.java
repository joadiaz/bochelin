package com.bochelin.repositories;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.PresentationDAO;
import com.bochelin.domain.Presentation;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("presentationRepository")
public class PresentationRepository extends AbstractDAO<Integer, Presentation> implements PresentationDAO {

    public void saveEntity(Presentation presentation) {
        persist(presentation);
    }

    public void deleteEntity(Presentation presentation) {
        delete(presentation);
    }

    public List<Presentation> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public Presentation findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Presentation) criteria.uniqueResult();
    }
}
