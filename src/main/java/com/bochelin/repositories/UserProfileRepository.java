package com.bochelin.repositories;


import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.UserProfileDAO;
import com.bochelin.domain.UserProfile;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userProfileRepository")
public class UserProfileRepository extends AbstractDAO<Integer, UserProfile> implements UserProfileDAO{

    @SuppressWarnings("unchecked")
    public List<UserProfile> findAll(){
        Criteria criteria = createEntityCriteria();
        criteria.addOrder(Order.asc("type"));
        return (List<UserProfile>)criteria.list();
    }

    public UserProfile findById(int id) {
        return getByKey(id);
    }

    public UserProfile findByType(String type) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("type", type));
        return (UserProfile) criteria.uniqueResult();
    }
}
