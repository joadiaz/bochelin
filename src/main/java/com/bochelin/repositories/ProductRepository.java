package com.bochelin.repositories;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.ProductDAO;
import com.bochelin.domain.Product;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("productRepository")
public class ProductRepository extends AbstractDAO<Integer, Product> implements ProductDAO{

    @Override
    public void saveEntity(Product product) {
        persist(product);
    }

    @Override
    public void deleteEntity(Product product) {
        delete(product);
    }

    @Override
    public List<Product> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.eq("isCustom", false));
        return (List<Product>) criteria.list();
    }

    @Override
    public Product findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Product) criteria.uniqueResult();
    }

    public Product getLastAddedProduct(){
        Criteria criteria = createEntityCriteria();
        criteria.addOrder(Order.desc("id")).setMaxResults(1);
        return (Product) criteria.uniqueResult();
    }

    public List<Product> findAllCustomProducts(){
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.eq("isCustom", true));
        return (List<Product>) criteria.list();
    }

}
