package com.bochelin.repositories;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.BillDetailsDAO;
import com.bochelin.domain.BillDetail;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class BillDetailsRepository extends AbstractDAO<Integer, BillDetail> implements BillDetailsDAO{

    @Override
    public void saveEntity(BillDetail billDetail) {
        persist(billDetail);
    }

    @Override
    public void deleteEntity(BillDetail billDetail) {
        delete(billDetail);
    }

    @Override
    public List<BillDetail> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public BillDetail findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (BillDetail) criteria.uniqueResult();
    }
}
