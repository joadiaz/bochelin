package com.bochelin.repositories;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.ClientsDAO;
import com.bochelin.domain.Client;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("clientRepository")
public class ClientRepository extends AbstractDAO<Integer, Client> implements ClientsDAO {

    @Override
    public void saveEntity(Client client) {
        persist(client);
    }

    @Override
    public void deleteEntity(Client client) {
        delete(client);
    }

    @Override
    public List<Client> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }
    @Override
    public Client findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Client) criteria.uniqueResult();
    }

    public Client findByDNI(int dni) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("dni", dni));
        return (Client) criteria.uniqueResult();
    }

    public Client getLastAddedClient(){
        Criteria criteria = createEntityCriteria();
        criteria.addOrder(Order.desc("id")).setMaxResults(1);
        return (Client) criteria.uniqueResult();
    }

}
