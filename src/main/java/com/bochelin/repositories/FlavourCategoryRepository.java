package com.bochelin.repositories;

import com.bochelin.dao.AbstractDAO;
import com.bochelin.dao.FlavourCategoryDAO;
import com.bochelin.domain.FlavourCategory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("flavourCategoryRepository")
public class FlavourCategoryRepository extends AbstractDAO<Integer, FlavourCategory> implements FlavourCategoryDAO {

    @Override
    public void saveEntity(FlavourCategory flavourCategory) {
        persist(flavourCategory);
    }

    @Override
    public void deleteEntity(FlavourCategory flavourCategory){
        delete(flavourCategory);
    }

    @Override
    public List<FlavourCategory> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public FlavourCategory findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (FlavourCategory) criteria.uniqueResult();
    }
}
