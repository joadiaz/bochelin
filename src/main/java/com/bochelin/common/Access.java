package com.bochelin.common;

public enum Access {
	PUBLIC, MANAGER, OWNER
}
