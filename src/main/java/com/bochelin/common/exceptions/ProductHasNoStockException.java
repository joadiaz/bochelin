package com.bochelin.common.exceptions;

public class ProductHasNoStockException extends GenericException {
    public ProductHasNoStockException(String productName) {
        super("P1", productName + " no tiene stock");
    }
}
