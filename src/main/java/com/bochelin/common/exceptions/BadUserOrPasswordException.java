package com.bochelin.common.exceptions;

public class BadUserOrPasswordException extends GenericException {
	private static final long serialVersionUID = 1L;
	public BadUserOrPasswordException(){
		super("U3", "Usuario o contraseña incorrecto");
	}
} 