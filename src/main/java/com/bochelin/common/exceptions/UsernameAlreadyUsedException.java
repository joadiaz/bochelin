package com.bochelin.common.exceptions;

public class UsernameAlreadyUsedException extends GenericException {
    public UsernameAlreadyUsedException(){
        super("U1", "Username already in use");
    }
}