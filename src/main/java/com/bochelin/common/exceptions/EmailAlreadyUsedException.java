package com.bochelin.common.exceptions;

public class EmailAlreadyUsedException extends GenericException {
    public EmailAlreadyUsedException() {
        super("U4", "Email already in use");
    }
}
