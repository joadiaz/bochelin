package com.bochelin.common.exceptions;

public class FlavourHasNoStockException extends GenericException{
    public FlavourHasNoStockException(String flavourName) {
        super("F1", flavourName + " no tiene stock");
    }
}
