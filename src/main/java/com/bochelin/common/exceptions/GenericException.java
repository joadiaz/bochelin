package com.bochelin.common.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

public class GenericException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    private String code;
    private String exceptionMessage;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> errorDetails = new ArrayList<String>();

    public GenericException(String code, String exceptionMessage) {
        this.code = code;
        this.exceptionMessage = exceptionMessage;
    }

    public GenericException(String code, String exceptionMessage, List<String> errorDetails) {
        this.code = code;
        this.exceptionMessage = exceptionMessage;
        this.errorDetails = errorDetails;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public List<String> getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(List<String> errorDetails) {
        this.errorDetails = errorDetails;
    }

}
