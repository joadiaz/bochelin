package com.bochelin.common.exceptions;

public class FileUploadException extends GenericException {
    public FileUploadException(String uploadError) {
        super("F1", uploadError);
    }
}
