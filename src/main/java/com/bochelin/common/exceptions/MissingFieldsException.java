package com.bochelin.common.exceptions;

import java.util.List;

/**
 * Created by joaquin on 27/05/16.
 */
public class MissingFieldsException extends GenericException {
    public MissingFieldsException() {
        super("G1", "Existen campos faltantes");
    }
    public MissingFieldsException(List<String> errorDetails) {
        super("G1", "Existen campos faltantes", errorDetails);
    }
}
