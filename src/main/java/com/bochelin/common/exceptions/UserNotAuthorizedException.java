package com.bochelin.common.exceptions;

public class UserNotAuthorizedException extends GenericException {
	public UserNotAuthorizedException(){
		super("U2", "User Not Authorized");
	}
} 