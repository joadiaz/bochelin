package com.bochelin.domain;

public class FilePath {

    private String path;

    public FilePath(String path){
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
