package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DailySell {

    private static final Logger logger = Logger.getLogger(DailySell.class);

    @JsonView(Views.Summary.class)
    private Date sellDate = null;

    @JsonView(Views.Summary.class)
    private float quantity = 0.0f;

    @JsonView(Views.Summary.class)
    private List<DailyProductSell> dailyProductSellList = new ArrayList<DailyProductSell>();

    @JsonView(Views.Summary.class)
    private List<DailyProductSell> dailyCustomProductSellList = new ArrayList<DailyProductSell>();

    public Date getSellDate() {
        return sellDate;
    }

    public void setSellDate(Date sellDate) {
        this.sellDate = sellDate;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public List<DailyProductSell> getDailyProductSellList() {
        return dailyProductSellList;
    }

    public void setDailyProductSellList(List<DailyProductSell> dailyProductSellList) {
        this.dailyProductSellList = dailyProductSellList;
    }

    public List<DailyProductSell> getDailyCustomProductSellList() {
        return dailyCustomProductSellList;
    }

    public void setDailyCustomProductSellList(List<DailyProductSell> dailyCustomProductSellList) {
        this.dailyCustomProductSellList = dailyCustomProductSellList;
    }

    public void addProductsToList(List<BillDetail> billDetaillist){
        logger.debug("ADDING PRODUCTS");
        for (int i = 0; i < billDetaillist.size(); i++){
            Boolean added = false;
            Product product = billDetaillist.get(i).getProduct();
            if (product.isCustom()) {
                logger.debug("CUSTOM PRODUCT");
                for (int j = 0; j < this.dailyCustomProductSellList.size() && !added ; j++) {
                    if (product.equals(dailyCustomProductSellList.get(j).getDailyProduct())) {
                        logger.debug("ADDED");
                        added = true;
                        dailyCustomProductSellList.get(j).setQuantity(dailyCustomProductSellList.get(j).getQuantity() + billDetaillist.get(i).getQuantity());
                    }
                }
                if (!added) {
                    logger.debug("NEW CUSTOM");
                    DailyProductSell newDailyProductSell = new DailyProductSell();
                    newDailyProductSell.setDailyProduct(product);
                    newDailyProductSell.setQuantity(billDetaillist.get(i).getQuantity());
                    dailyCustomProductSellList.add(newDailyProductSell);
                }
            } else {
                added = false;
                for (int j = 0; j < this.dailyProductSellList.size() && !added ; j++) {
                    if (product.equals(dailyProductSellList.get(j).getDailyProduct())) {
                        added = true;
                        dailyProductSellList.get(j).setQuantity(dailyProductSellList.get(j).getQuantity() + billDetaillist.get(i).getQuantity());
                    }
                }

                if (!added) {
                    DailyProductSell newDailyProductSell = new DailyProductSell();
                    newDailyProductSell.setDailyProduct(product);
                    newDailyProductSell.setQuantity(billDetaillist.get(i).getQuantity());
                    dailyProductSellList.add(newDailyProductSell);
                }
            }
        }
    }
}
