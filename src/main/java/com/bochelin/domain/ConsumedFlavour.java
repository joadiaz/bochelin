package com.bochelin.domain;

public class ConsumedFlavour {

    private int flavourId = 0;
    private String flavourName = "";
    private float consumedQuantity = 0.0f;

    public int getFlavourId() {
        return flavourId;
    }

    public void setFlavourId(int flavourId) {
        this.flavourId = flavourId;
    }

    public String getFlavourName() {
        return flavourName;
    }

    public void setFlavourName(String flavourName) {
        this.flavourName = flavourName;
    }

    public float getConsumedQuantity() {
        return consumedQuantity;
    }

    public void setConsumedQuantity(float consumedQuantity) {
        this.consumedQuantity = consumedQuantity;
    }
}
