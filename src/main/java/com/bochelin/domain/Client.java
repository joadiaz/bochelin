package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "clients")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@jsonId")
public class Client {

    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonView(Views.Summary.class)
    private int id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "client", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Bill> bills = new ArrayList<Bill>();

    @Column(name = "dni", nullable = false, unique = true)
    @JsonView(Views.Summary.class)
    private int dni = 0;

    @Column(name = "name", nullable = false)
    @JsonView(Views.Summary.class)
    private String name = null;

    @Column(name = "address", nullable = false)
    @JsonView(Views.Summary.class)
    private String address = null;

    @Column(name = "email", nullable = false)
    @JsonView(Views.Summary.class)
    private String email = null;

    @Column(name = "phone", nullable = false)
    @JsonView(Views.Summary.class)
    private String phone = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
