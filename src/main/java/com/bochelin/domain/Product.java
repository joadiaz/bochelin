package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "products")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@jsonId")
public class Product extends BaseEntity {

    @ManyToOne()
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "presentation_id", nullable = false)
    @JsonView(Views.ProductSumary.class)
    private Presentation presentation = new Presentation();

    @JsonView(Views.Summary.class)
    @Column(name = "price", nullable = false)
    private Float price = 0.0f;

    @ManyToMany()
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "products_flavours",
               joinColumns = @JoinColumn(name = "product_id"),
               inverseJoinColumns = @JoinColumn(name = "flavour_id"))
    @JsonView(Views.ProductSumary.class)
    private List<Flavour> flavours = new ArrayList<Flavour>();

    @JsonView(Views.Summary.class)
    @Column(name = "stock", nullable = false)
    private int stock = 0;

    @JsonView(Views.Summary.class)
    @Column(name = "is_custom")
    private boolean isCustom = false;

    public Presentation getPresentation() {
        return presentation;
    }

    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public List<Flavour> getFlavours() {
        return flavours;
    }

    public void setFlavours(List<Flavour> flavours) {
        this.flavours = flavours;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public boolean isCustom() {
        return isCustom;
    }

    public void setCustom(boolean custom) {
        isCustom = custom;
    }

    public Boolean equals(Product otherProduct){
        return otherProduct.getId() == this.getId();
    }

}
