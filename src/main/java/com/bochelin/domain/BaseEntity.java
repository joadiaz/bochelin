package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonView(Views.Summary.class)
    protected int id;

    @NotNull(message = "El nombre no puede ser nulo")
    @Size(max=100)
    @Column(name = "name", nullable = false)
    @JsonView(Views.Summary.class)
    protected String name;

    @Size(max=255)
    @Column(name = "description", nullable = false)
    @JsonView(Views.Summary.class)
    protected String description;

    @Size(max=100)
    @Column(name = "image")
    @JsonView(Views.Summary.class)
    protected String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
