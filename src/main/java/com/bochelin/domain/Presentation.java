package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "presentations")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@jsonId")
public class Presentation extends BaseEntity {

    @JsonView(Views.PresentationSummary.class)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "presentation", cascade= CascadeType.REMOVE)
    private List<Product> products = new ArrayList<Product>();

    @JsonView(Views.Summary.class)
    @Column(name = "max_flavours", nullable = false)
    private int maxFlavours = 1;

    @JsonView(Views.Summary.class)
    @Column(name = "is_customizable")
    private boolean isCustomizable = false;

    @JsonView(Views.Summary.class)
    @Column(name = "price", nullable = false)
    private Float price = 0.0f;

    @Column(name = "weight", nullable = false)
    @JsonView(Views.Summary.class)
    private Float weight = 0.0f;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getMaxFlavours() {
        return maxFlavours;
    }

    public void setMaxFlavours(int maxFlavours) {
        this.maxFlavours = maxFlavours;
    }

    public boolean isCustomizable() {
        return isCustomizable;
    }

    public void setCustomizable(boolean customizable) {
        isCustomizable = customizable;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }
}
