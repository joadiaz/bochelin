package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "flavours")
public class Flavour extends BaseEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", nullable = false)
    @JsonView(Views.FlavourSummary.class)
    private FlavourCategory flavourCategory = new FlavourCategory();

    @Column(name = "stock", nullable = false)
    @JsonView(Views.FlavourSummary.class)
    private Float stock = 0.0f;

    @ManyToMany(mappedBy = "flavours")
    private List<Product> products = new ArrayList<Product>();

    public FlavourCategory getFlavourCategory() {
        return flavourCategory;
    }

    public void setFlavourCategory(FlavourCategory flavourCategory) {
        this.flavourCategory = flavourCategory;
    }

    public Float getStock() {
        return stock;
    }

    public void setStock(Float stock) {
        this.stock = stock;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public boolean equals(Flavour otherFlavour){
        return this.getId() == otherFlavour.getId();
    }
}
