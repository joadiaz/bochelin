package com.bochelin.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bochelin.common.UserState;
import com.bochelin.domain.UserProfile;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@NotNull(message = "El usuario no puede ser nulo")
	@Size(min=3, max=50)
	@Column(name = "username", nullable = false, unique = true)
	private String username;

	@NotNull(message = "La contraseña no puede ser nula")
	@Column(name = "password", nullable = false)
	private String password;

	@NotNull
	@Column(name = "state", nullable=false)
	private String state = UserState.ACTIVE.getState();

	@NotNull(message = "El email no puede ser nulo")
	@Size(max=100)
	@Column(name = "email", nullable = false, unique = true)
	private String email;

	@NotNull(message = "El nombre no puede ser nulo")
	@Size(max=100)
	@Column(name = "name", nullable = false)
	private String name;

	@NotNull(message = "El apellido no puede ser nulo")
	@Size(max=100)
	@Column(name = "lastname", nullable = false)
	private String lastname;

	@Size(max=100)
	@Column(name = "telephone")
	private String telephone;

	@Size(max=100)
	@Column(name = "address")
	private String address;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_profile_id")
	private UserProfile userProfile = new UserProfile();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
