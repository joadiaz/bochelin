package com.bochelin.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "flavour_categories")
public class FlavourCategory extends BaseEntity {

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "flavourCategory", cascade= CascadeType.REMOVE)
    private List<Flavour> flavours = new ArrayList<Flavour>();

    public List<Flavour> getFlavours() {
        return flavours;
    }

    public void setFlavours(List<Flavour> flavours) {
        this.flavours = flavours;
    }
}
