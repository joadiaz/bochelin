package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.ArrayList;
import java.util.List;

public class Report {

    @JsonView(Views.Summary.class)
    private float total = 0.0f;

    @JsonView(Views.Summary.class)
    private List<DailySell> dailySellList = new ArrayList<DailySell>();

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public List<DailySell> getDailySellList() {
        return dailySellList;
    }

    public void setDailySellList(List<DailySell> dailySellList) {
        this.dailySellList = dailySellList;
    }
}
