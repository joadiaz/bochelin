package com.bochelin.domain;

import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;

public class DailyProductSell {

    @JsonView(Views.Summary.class)
    private Product dailyProduct = new Product();

    @JsonView(Views.Summary.class)
    private int quantity = 0;

    public Product getDailyProduct() {
        return dailyProduct;
    }

    public void setDailyProduct(Product dailyProduct) {
        this.dailyProduct = dailyProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
