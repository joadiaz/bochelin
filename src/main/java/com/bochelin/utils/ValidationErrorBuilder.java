package com.bochelin.utils;

import com.bochelin.common.exceptions.ValidationErrorMessage;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class ValidationErrorBuilder {

    public static ValidationErrorMessage fromBindingErrors(Errors errors) {
        ValidationErrorMessage error = new ValidationErrorMessage("Validation failed. " + errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}
