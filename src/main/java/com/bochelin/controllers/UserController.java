package com.bochelin.controllers;

import com.bochelin.common.GenericMessage;
import com.bochelin.common.exceptions.EmailAlreadyUsedException;
import com.bochelin.common.exceptions.GenericErrorMessage;
import com.bochelin.common.exceptions.GenericException;
import com.bochelin.common.exceptions.UsernameAlreadyUsedException;
import com.bochelin.domain.User;
import com.bochelin.services.UserProfileService;
import com.bochelin.services.UserService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/users")
public class UserController {

    private static final Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Autowired
    MessageSource messageSource;

    @Autowired
    UserProfileService userProfileService;

    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    @ResponseBody
    public List<User> listUsers() {
        List<User> users = userService.findAllUsers();
        return users;
    }

    @RequestMapping(value = { "/create"}, method = RequestMethod.POST)
    @ResponseBody
    public GenericMessage createUser(@RequestBody @Valid User user) throws UsernameAlreadyUsedException, EmailAlreadyUsedException {
        try {
            userService.saveUser(user);
        } catch (DataIntegrityViolationException e){
            logger.debug("Username already used");
            if(e.getCause().getClass().getName() == "org.hibernate.exception.ConstraintViolationException"){
                if (userService.findByUsername(user.getUsername()) != null){
                    throw new UsernameAlreadyUsedException();
                }
                if (userService.findByEmail(user.getEmail()) != null){
                    throw new EmailAlreadyUsedException();
                }
            }
        } catch (Exception e){
            throw new GenericException("U3", e.getMessage());
        }

        logger.debug("User created succesfully");
        return new GenericMessage("User created succesfully");
    }


    @ExceptionHandler(UsernameAlreadyUsedException.class)
    @ResponseBody
    public GenericErrorMessage handleUsernameAlreadyUsedException(UsernameAlreadyUsedException ex) {
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

    @ExceptionHandler(EmailAlreadyUsedException.class)
    @ResponseBody
    public GenericErrorMessage handleEmailAlreadyUsedException(EmailAlreadyUsedException ex) {
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

    @ExceptionHandler(GenericException.class)
    @ResponseBody
    public GenericErrorMessage handleGenericException(GenericException ex) {
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

}
