package com.bochelin.controllers;


import com.bochelin.domain.Presentation;
import com.bochelin.domain.Product;
import com.bochelin.services.ProductService;
import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/products")
public class ProductController implements ABM<Product> {

    private static final Logger logger = Logger.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public void create(@RequestBody @Valid Product product) {
        productService.save(product);
    }

    @RequestMapping(value = { "" }, method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody @Valid Product product) {
        productService.update(product);
    }

    @RequestMapping(value = { "/{id}" }, method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable(value="id") int id) {
        productService.delete(id);
    }

    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.Summary.class)
    public List<Product> list() {
        return productService.findAll();
    }

    @RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.ProductSumary.class)
    public Product findById(@PathVariable(value="id") int id) {
        return productService.findById(id);
    }

    @RequestMapping(value = { "/update-stock" }, method = RequestMethod.POST)
    @ResponseBody
    public void updateStock(@RequestBody @Valid Product product) {
        productService.updateStock(product);
    }

}
