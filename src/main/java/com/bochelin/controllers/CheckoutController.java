package com.bochelin.controllers;

import com.bochelin.common.GenericMessage;
import com.bochelin.common.exceptions.FlavourHasNoStockException;
import com.bochelin.common.exceptions.GenericErrorMessage;
import com.bochelin.common.exceptions.ProductHasNoStockException;
import com.bochelin.domain.Bill;
import com.bochelin.repositories.BillRepository;
import com.bochelin.services.BillService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/api/checkout")
public class CheckoutController {

    private static final Logger logger = Logger.getLogger(FlavourCategoryController.class);

    @Autowired
    BillService billService;

    @ResponseBody
    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    public GenericMessage doCheckout(@RequestBody @Valid Bill bill)
            throws ProductHasNoStockException, FlavourHasNoStockException{
        try {
            billService.doCheckout(bill);
        } catch (ProductHasNoStockException ex) {
            throw ex;
        } catch (FlavourHasNoStockException ex) {
            throw ex;
        }
        return new GenericMessage("Sold!");
    }

    @ExceptionHandler(ProductHasNoStockException.class)
    @ResponseBody
    public GenericErrorMessage handleProductHasNoStockException(ProductHasNoStockException ex) {
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

    @ExceptionHandler(FlavourHasNoStockException.class)
    @ResponseBody
    public GenericErrorMessage handleFlavourHasNoStockException(FlavourHasNoStockException ex) {
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

}
