package com.bochelin.controllers;

import com.bochelin.domain.Presentation;
import com.bochelin.services.PresentationService;
import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/products/presentation")
public class PresentationController implements ABM<Presentation> {

    @Autowired
    PresentationService presentationService;

    @RequestMapping(value = {""}, method = RequestMethod.POST)
    @ResponseBody
    public void create(@RequestBody @Valid Presentation presentation) {
        presentationService.save(presentation);
    }

    @RequestMapping(value = {""}, method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody @Valid Presentation presentation) {
        presentationService.update(presentation);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable(value = "id") int id) {
        presentationService.delete(id);
    }

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.Summary.class)
    public List<Presentation> list() {
        return presentationService.findAll();
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.PresentationSummary.class)
    public Presentation findById(@PathVariable(value = "id") int id) {
        return presentationService.findById(id);
    }
}


