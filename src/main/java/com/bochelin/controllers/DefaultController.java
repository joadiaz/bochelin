package com.bochelin.controllers;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Component
@RequestMapping("/")
public class DefaultController {


    @RequestMapping(value= "*", method = RequestMethod.GET)
    public String getSitePage() {
        System.out.print("index");
        return "index";
    }

    @RequestMapping(value= "admin/*/**", method = RequestMethod.GET)
    public String getAdminPages() {
        System.out.print("index");
        return "index";
    }

    @RequestMapping(value= "admin/*/*/**", method = RequestMethod.GET)
    public String getAdminEditPages() {
        System.out.print("index");
        return "index";
    }

    @RequestMapping(value= "shop/*/**", method = RequestMethod.GET)
    public String getSiteEditPages() {
        System.out.print("index");
        return "index";
    }

}