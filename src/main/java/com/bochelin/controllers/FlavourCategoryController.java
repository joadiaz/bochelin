package com.bochelin.controllers;

import com.bochelin.domain.FlavourCategory;
import com.bochelin.services.FlavourCategoryService;
import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/flavours/category")
public class FlavourCategoryController implements ABM<FlavourCategory> {

    private static final Logger logger = Logger.getLogger(FlavourCategoryController.class);

    @Autowired
    FlavourCategoryService flavourCategoryService;

    @Override
    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public void create(@RequestBody @Valid FlavourCategory flavourCategory) {
        flavourCategoryService.save(flavourCategory);
    }

    @Override
    @RequestMapping(value = { "" }, method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody @Valid FlavourCategory flavourCategory) {
        flavourCategoryService.update(flavourCategory);
    }

    @Override
    @RequestMapping(value = { "/{id}" }, method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable(value="id") int id) {
        flavourCategoryService.delete(id);
    }

    @Override
    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.Summary.class)
    public List<FlavourCategory> list() {
        return flavourCategoryService.findAll();
    }

    @RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.Summary.class)
    public FlavourCategory findById(@PathVariable(value="id") int id) {
        return flavourCategoryService.findById(id);
    }
}
