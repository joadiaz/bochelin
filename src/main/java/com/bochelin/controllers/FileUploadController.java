package com.bochelin.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bochelin.common.GenericMessage;
import com.bochelin.common.exceptions.FileUploadException;
import com.bochelin.common.exceptions.GenericErrorMessage;
import com.bochelin.common.exceptions.UsernameAlreadyUsedException;
import com.bochelin.domain.FilePath;
import com.bochelin.services.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
@Component
@RequestMapping("/api/upload")
public class FileUploadController {

    @Autowired
    FileUploadService fileUploadService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public FilePath handleFileUpload(@RequestParam("file") MultipartFile file) throws FileUploadException {
        if (!file.isEmpty()) {
            try {
                return new FilePath(fileUploadService.uploadFile(file));
            } catch(FileUploadException ex){
                throw ex;
            }
        } else {
            throw new FileUploadException("No hay ningún archivo");
        }
    }

    @ExceptionHandler(FileUploadException.class)
    @ResponseBody
    public GenericErrorMessage handleFileUploadException(FileUploadException ex) {
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

}