package com.bochelin.controllers;

import java.util.List;

public interface ABM<T> {

    void create(T entity);

    void update(T entity);

    void delete(int id);

    List<T> list();

}
