package com.bochelin.controllers;

import com.bochelin.domain.Flavour;
import com.bochelin.services.FlavourService;
import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/flavours")
public class FlavourController implements ABM<Flavour> {

    private static final Logger logger = Logger.getLogger(FlavourController.class);

    @Autowired
    FlavourService flavourService;

    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public void create(@RequestBody @Valid Flavour flavour) {
        flavourService.save(flavour);
    }

    @RequestMapping(value = { "" }, method = RequestMethod.PUT)
    @ResponseBody
    public void update(@RequestBody @Valid Flavour flavour) {
        flavourService.update(flavour);
    }

    @RequestMapping(value = { "/{id}" }, method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable(value="id") int id) {
        flavourService.delete(id);
    }

    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.FlavourSummary.class)
    public List<Flavour> list() {
        return flavourService.findAll();
    }

    @RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.FlavourSummary.class)
    public Flavour findById(@PathVariable(value="id") int id) {
        return flavourService.findById(id);
    }

    @RequestMapping(value = { "/search/{filter}" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.FlavourSummary.class)
    public List<Flavour> search(@PathVariable(value="filter") String filter){
        return flavourService.search(filter);
    }

    @RequestMapping(value = { "/search/{category}/{filter}" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.FlavourSummary.class)
    public List<Flavour> searchByCategory(
            @PathVariable(value="category") int categoryId,
            @PathVariable(value="filter") String filter){
        return flavourService.searchByCategory(categoryId, filter);
    }

    @RequestMapping(value = { "/update-stock" }, method = RequestMethod.POST)
    @ResponseBody
    public void updateStock(@RequestBody @Valid Flavour flavour) {
        flavourService.updateStock(flavour);
    }

}
