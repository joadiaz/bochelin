package com.bochelin.controllers;

import com.bochelin.domain.Report;
import com.bochelin.services.ReportService;
import com.bochelin.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/reports")
public class ReportController {

    private static final Logger logger = Logger.getLogger(ReportController.class);

    @Autowired
    ReportService reportService;

    @RequestMapping(value = { "/{month}" }, method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Views.FlavourSummary.class)
    public Report getMonthlyReport(@PathVariable(value="month") int month){
        return reportService.getMonthlyReport(month);
    }

}
