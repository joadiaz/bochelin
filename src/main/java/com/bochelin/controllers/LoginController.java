package com.bochelin.controllers;

import com.bochelin.common.LoginResponse;
import com.bochelin.common.exceptions.BadUserOrPasswordException;
import com.bochelin.domain.User;
import com.bochelin.services.UserService;
import com.bochelin.utils.PasswordManagment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

@Controller
@RequestMapping("/api/login")
public class LoginController {

    private static final Logger logger = Logger.getLogger(LoginController.class);

    @Autowired
    UserService userService;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public LoginResponse login(@RequestBody User user) throws BadUserOrPasswordException {

        User dbUser = userService.findByUsername(user.getUsername());

        if(dbUser == null){
            throw new BadUserOrPasswordException();
        }

        if(!PasswordManagment.checkPassword(user.getPassword(), dbUser.getPassword())){
            throw new BadUserOrPasswordException();
        }

        return new LoginResponse(Jwts.builder().setSubject(user.getUsername())
                .claim("roles", "admin").setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact(), dbUser.getUserProfile().getType());

    }

}
