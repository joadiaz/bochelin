package com.bochelin.services;

import com.bochelin.domain.Flavour;
import com.bochelin.repositories.FlavourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("flavourService")
@Transactional
public class FlavourServiceImpl implements FlavourService {

    @Autowired
    FlavourRepository flavourRepository;

    @Override
    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void save(Flavour flavour) {
        flavourRepository.saveEntity(flavour);
    }

    @Override
    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void update(Flavour flavour) {
        Flavour dbItem = flavourRepository.findById(flavour.getId());
        if(dbItem != null){
            dbItem.setName(flavour.getName());
            dbItem.setDescription(flavour.getDescription());
            dbItem.setImage(flavour.getImage());
            dbItem.setStock(flavour.getStock());
            if(flavour.getFlavourCategory() != null){
                dbItem.setFlavourCategory(flavour.getFlavourCategory());
            }
        }
    }

    @Override
    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void delete(int id) {
        Flavour dbItem = flavourRepository.findById(id);
        if(dbItem != null){
            flavourRepository.deleteEntity(dbItem);
        }
    }

    @Override
    public Flavour findById(int id) {
        return flavourRepository.findById(id);
    }

    @Override
    public List<Flavour> findAll() {
        return flavourRepository.findAll();
    }

    @Override
    public List<Flavour> search(String filter){
        return flavourRepository.search(filter);
    }

    @Override
    public List<Flavour> searchByCategory(int categoryId, String filter){
        return flavourRepository.searchByCategory(categoryId, filter);
    }

    @Override
    public Boolean flavourHasStock(int flavourId, Float quantity){
        if (flavourRepository.findById(flavourId).getStock() < quantity) {
            return false;
        }
        return true;
    }

    @Override
    @PreAuthorize(value="isAuthenticated() and hasAnyRole('ROLE_MANAGER','ROLE_SALESMAN')")
    public void updateStock(Flavour flavour){
        Flavour dbItem = flavourRepository.findById(flavour.getId());
        if (dbItem!=null) {
            dbItem.setStock(flavour.getStock());
        }
    }

    public void reduceFlavourStock(Flavour flavour, float weightPerFlavour){
        Flavour dbItem = flavourRepository.findById(flavour.getId());
        if (dbItem != null) {
            dbItem.setStock(dbItem.getStock() - weightPerFlavour);
        }
    }

}
