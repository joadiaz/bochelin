package com.bochelin.services;

import com.bochelin.dao.UserProfileDAO;
import com.bochelin.domain.UserProfile;
import com.bochelin.repositories.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService{

    @Autowired
    UserProfileRepository repository;

    public List<UserProfile> findAll() {
        return repository.findAll();
    }

    public UserProfile findByType(String type){
        return repository.findByType(type);
    }

    public UserProfile findById(int id) {
        return repository.findById(id);
    }
}
