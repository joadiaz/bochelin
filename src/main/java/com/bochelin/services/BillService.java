package com.bochelin.services;

import com.bochelin.domain.Bill;
import java.util.List;

public interface BillService extends ABMService<Bill>{

    public Boolean doCheckout(Bill bill);

    public List<Bill> getMonthlyBills(int monthNumber);

}
