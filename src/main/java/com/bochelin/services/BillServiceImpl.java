package com.bochelin.services;

import com.bochelin.common.exceptions.FlavourHasNoStockException;
import com.bochelin.common.exceptions.ProductHasNoStockException;
import com.bochelin.domain.*;
import com.bochelin.repositories.BillRepository;
import com.bochelin.repositories.ProductRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("billService")
@Transactional
public class BillServiceImpl implements BillService {

    private static final Logger logger = Logger.getLogger(BillServiceImpl.class);

    @Autowired
    BillRepository billRepository;

    @Autowired
    ProductService productService;

    @Autowired
    FlavourService flavourService;

    @Autowired
    PresentationService presentationService;

    @Autowired
    ClientService clientService;

    public void save(Bill bill) {
        List<BillDetail> notSavedBillDetails = bill.getBillDetails();
        bill.setBillDetails(null);
        bill.setClient(clientService.getOrCreateClient(bill.getClient()));
        billRepository.saveEntity(bill);
        Bill dbItem = billRepository.getLastAddedBill();

        for (int i = 0; i < notSavedBillDetails.size(); i++){
            notSavedBillDetails.get(i).setBill(dbItem);
        }
        bill.setBillDetails(notSavedBillDetails);
        this.update(bill);
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void update(Bill bill) {
        Bill dbItem = billRepository.findById(bill.getId());
        if(dbItem != null){
            if(dbItem.getBillDetails().size() > 0){
                dbItem.setBillDetails(bill.getBillDetails());
            }
        }
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void delete(int id) {
        Bill dbItem = billRepository.findById(id);
        if(dbItem != null){
            billRepository.delete(dbItem);
        }
    }

    public Bill findById(int id) {
        return billRepository.findById(id);
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public List<Bill> findAll() {
        return billRepository.findAll();
    }

    public Boolean doCheckout(Bill bill) throws ProductHasNoStockException, FlavourHasNoStockException{
        this.checkStocks(bill.getBillDetails());
        this.reduceStocks(bill.getBillDetails());
        bill.setBillDate(new Date());
        this.save(bill);
        return false;
    }

    //@PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public List<Bill> getMonthlyBills(int monthNumber) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MONTH, monthNumber-1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date fromDate = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MONTH, monthNumber-1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date toDate = calendar.getTime();

        return billRepository.getBillsBetweenDates(fromDate, toDate);
    }

    private void checkStocks(List<BillDetail> billDetails) throws ProductHasNoStockException, FlavourHasNoStockException {
        List<ConsumedFlavour> consumedFlavours = new ArrayList<ConsumedFlavour>();
        for (int i = 0; i < billDetails.size(); i++){
            Product productInDetail = billDetails.get(i).getProduct();
            if (!productInDetail.isCustom()) {
                if (!productService.productHasStock(productInDetail.getId(), billDetails.get(i).getQuantity())){
                    throw new ProductHasNoStockException(productInDetail.getName());
                }
            } else {
                for (int j = 0; j < productInDetail.getFlavours().size(); j++){
                    billDetails.get(i).setProduct(productService.getCustomProduct(billDetails.get(i).getProduct()));
                    productInDetail = billDetails.get(i).getProduct();
                    Float weightPerFlavour = presentationService.getWeightPerFlavour(productInDetail.getPresentation().getId(), productInDetail.getFlavours().size());
                    Flavour flavourInProduct = productInDetail.getFlavours().get(j);
                    boolean consumedFlavourSaved = false;
                    for (int k = 0; k < consumedFlavours.size(); k++){
                        if (consumedFlavours.get(k).getFlavourId() == flavourInProduct.getId()){
                            consumedFlavours.get(k).setConsumedQuantity(consumedFlavours.get(k).getConsumedQuantity() + weightPerFlavour);
                            consumedFlavourSaved = true;
                        }
                    }

                    if (!consumedFlavourSaved) {
                        ConsumedFlavour consumedFlavour = new ConsumedFlavour();
                        consumedFlavour.setFlavourId(flavourInProduct.getId());
                        consumedFlavour.setFlavourName(flavourInProduct.getName());
                        consumedFlavour.setConsumedQuantity(weightPerFlavour);
                        consumedFlavours.add(consumedFlavour);
                    }

                }
            }
        }

        for (int i = 0; i < consumedFlavours.size(); i++){
            if (!flavourService.flavourHasStock(consumedFlavours.get(i).getFlavourId(), consumedFlavours.get(i).getConsumedQuantity())) {
                throw new FlavourHasNoStockException(consumedFlavours.get(i).getFlavourName());
            }
        }

    }

    private void reduceStocks(List<BillDetail> billDetails){
        for (int i = 0; i < billDetails.size(); i++){
            if (billDetails.get(i).getProduct().isCustom()) {
                Float weightPerFlavour = presentationService.getWeightPerFlavour(billDetails.get(i).getProduct().getPresentation().getId(), billDetails.get(i).getProduct().getFlavours().size());
                for (int j = 0; j < billDetails.get(i).getProduct().getFlavours().size(); j++){
                    flavourService.reduceFlavourStock(billDetails.get(i).getProduct().getFlavours().get(j), weightPerFlavour);
                }
            } else {
                productService.reduceProductStock(billDetails.get(i).getProduct(), billDetails.get(i).getQuantity());
            }
        }
    }
}
