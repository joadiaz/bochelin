package com.bochelin.services;

import java.util.List;

import com.bochelin.domain.User;

public interface UserService {

    User findByUsername(String username);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUser(User user);

    List<User> findAllUsers();

    User findByEmail(String email);

}
