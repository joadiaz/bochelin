package com.bochelin.services;

import com.bochelin.domain.FlavourCategory;

public interface FlavourCategoryService extends ABMService<FlavourCategory> {
}
