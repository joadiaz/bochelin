package com.bochelin.services;

import com.bochelin.common.exceptions.FileUploadException;
import com.cloudinary.utils.ObjectUtils;
import com.sun.org.apache.xpath.internal.operations.Mult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import com.cloudinary.*;

import javax.servlet.ServletContext;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.springframework.util.MimeTypeUtils;
import org.apache.commons.lang3.RandomStringUtils;

@Service("fileUploadService")
public class FileUploadServiceImpl implements FileUploadService{

    @Autowired
    private ServletContext servletContext;

    private static String uploadPath = "/uploads/";
    private static final Logger logger = Logger.getLogger(FileUploadServiceImpl.class);

    @Override
    public String uploadFile(MultipartFile file) throws FileUploadException{
        logger.debug("Uploading file");
        if(file.getContentType().equals(MimeTypeUtils.IMAGE_JPEG_VALUE) ||
           file.getContentType().equals(MimeTypeUtils.IMAGE_PNG_VALUE)) {
            File fileToUpload = convertToFile(file);
            Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                    "cloud_name", "ddvaai9c9",
                    "api_key", "958366963365163",
                    "api_secret", "2DTKugu61n3XYvRV3qIt1FXM7II"));
            try {
                Map uploadResult = cloudinary.uploader().upload(fileToUpload, ObjectUtils.emptyMap());
                logger.debug("File upload");
                return (String) uploadResult.get("url");
            } catch (Exception e) {
                throw new FileUploadException(e.getMessage());
            }
        } else {
                logger.debug("Invalid mime type:" + file.getContentType());
                throw new FileUploadException("Tipo de archivo inválido");
        }
    }

    private String generateRandomName(){
        return RandomStringUtils.randomAlphanumeric(15);
    }

    private File convertToFile(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
        } catch (Exception e) {
            throw new FileUploadException(e.getMessage());
        }
        return convFile;
    }

}
