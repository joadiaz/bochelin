package com.bochelin.services;

import com.bochelin.domain.Flavour;
import com.bochelin.domain.Presentation;
import com.bochelin.domain.Product;
import com.bochelin.repositories.ProductRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {

    private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);

    @Autowired
    PresentationService presentationService;

    @Autowired
    ProductRepository productRepository;

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void save(Product product) {
        productRepository.saveEntity(product);
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void update(Product product) {
        Product dbItem = productRepository.findById(product.getId());
        if(dbItem != null){
            dbItem.setName(product.getName());
            dbItem.setDescription(product.getDescription());
            dbItem.setImage(product.getImage());
            dbItem.setPrice(product.getPrice());
            dbItem.setStock(product.getStock());
            dbItem.setCustom(product.isCustom());
            if(product.getPresentation() != null){
                dbItem.setPresentation(product.getPresentation());
            }
            if(product.getFlavours().size() > 0){
                dbItem.setFlavours(product.getFlavours());
            }
        }
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void delete(int id) {
        Product dbItem = productRepository.findById(id);
        if(dbItem != null){
            productRepository.delete(dbItem);
        }
    }

    public Product findById(int id) {
        return productRepository.findById(id);
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Boolean productHasStock(int id, int quantity){
        if (productRepository.findById(id).getStock() < quantity){
            return false;
        }
        return true;
    }

    public Product getCustomProduct(Product product){
        List<Product> productList = productRepository.findAllCustomProducts();
        Product existingCustomProduct = null;
        for (int i = 0; i < productList.size(); i++) {
            if (this.productHasTheSameFlavours(productList.get(i).getFlavours(), product.getFlavours())) {
                existingCustomProduct = productList.get(i);
                break;
            }
        }
        if (existingCustomProduct == null){
            product.setDescription("Producto creado por el cliente");
            product.setCustom(true);
            Presentation presentation = presentationService.findById(product.getPresentation().getId());
            product.setPresentation(presentation);
            product.setPrice(presentation.getPrice());
            this.save(product);
            return productRepository.getLastAddedProduct();
        }
        return existingCustomProduct;
    }

    public void reduceProductStock(Product product, int quantity){
        Product dbItem = productRepository.findById(product.getId());
        if (dbItem != null) {
            dbItem.setStock(dbItem.getStock() - quantity);
        }
    }

    @PreAuthorize(value="isAuthenticated() and hasAnyRole('ROLE_MANAGER','ROLE_SALESMAN')")
    public void updateStock(Product product) {
        Product dbItem = productRepository.findById(product.getId());
        if (dbItem != null){
            dbItem.setStock(product.getStock());
        }
    }

    private Boolean productHasTheSameFlavours(List<Flavour> firstFlavourList, List<Flavour> secondFlavourList){
        if (firstFlavourList.size() != secondFlavourList.size()) {
            return false;
        } else {
            Boolean sameFlavours = true;
            for (int i = 0; i < firstFlavourList.size() && sameFlavours; i++) {
                Boolean foundedFlavour = false;
                for (int j = 0; j < secondFlavourList.size() && !foundedFlavour; j++) {
                    if (firstFlavourList.get(i).equals(secondFlavourList.get(j))){
                        foundedFlavour = true;
                    }
                }
                if (!foundedFlavour) {
                    sameFlavours = false;
                }
            }
            return sameFlavours;
        }
    }


}

