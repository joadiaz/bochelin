package com.bochelin.services;

import com.bochelin.domain.Product;

public interface ProductService extends ABMService<Product> {

    public Boolean productHasStock(int productId, int quantity);

    public Product getCustomProduct(Product product);

    public void reduceProductStock(Product product, int quantity);

    public void updateStock(Product product);

}
