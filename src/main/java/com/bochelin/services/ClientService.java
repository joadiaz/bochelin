package com.bochelin.services;

import com.bochelin.domain.Client;

public interface ClientService extends ABMService<Client> {

    public Client getOrCreateClient(Client client);

}
