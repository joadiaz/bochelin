package com.bochelin.services;

import com.bochelin.domain.Presentation;

public interface PresentationService extends ABMService<Presentation> {

    public float getWeightPerFlavour(int id, int flavourQuantity);

}
