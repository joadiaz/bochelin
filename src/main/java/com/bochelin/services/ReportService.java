package com.bochelin.services;

import com.bochelin.domain.Report;

public interface ReportService {

    public Report getMonthlyReport(int monthNumber);

}
