package com.bochelin.services;

import com.bochelin.domain.Client;
import com.bochelin.repositories.ClientRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("clientService")
@Transactional
public class ClientServiceImpl implements ClientService {

    private static final Logger logger = Logger.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientRepository clientRepository;

    @Override
    public void save(Client client) {
        clientRepository.saveEntity(client);
    }

    @Override
    public void update(Client client) {
        Client dbItem = clientRepository.findById(client.getId());
        if (dbItem != null) {
            dbItem.setAddress(client.getAddress());
            dbItem.setDni(client.getDni());
            dbItem.setEmail(client.getEmail());
            dbItem.setName(client.getName());
            dbItem.setPhone(client.getPhone());
            if (client.getBills().size() > 0) {
                dbItem.setBills(client.getBills());
            }
        }
    }

    @Override
    public void delete(int id) {
        Client dbItem = clientRepository.findById(id);
        if (dbItem != null) {
            clientRepository.deleteEntity(dbItem);
        }
    }

    @Override
    public Client findById(int id) {
        return clientRepository.findById(id);
    }

    @Override
    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public Client getOrCreateClient(Client client) {
        Client dbItem = clientRepository.findByDNI(client.getDni());
        if (dbItem != null) {
            return dbItem;
        } else {
            this.save(client);
            return clientRepository.getLastAddedClient();
        }
    }

}
