package com.bochelin.services;

import com.bochelin.domain.FlavourCategory;
import com.bochelin.repositories.FlavourCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("flavourCategoryService")
@Transactional
public class FlavourCategoryServiceImpl implements FlavourCategoryService {

    @Autowired
    FlavourCategoryRepository flavourCategoryRepository;

    @Override
    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void save(FlavourCategory flavourCategory) {
        flavourCategoryRepository.saveEntity(flavourCategory);
    }

    @Override
    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void update(FlavourCategory flavourCategory) {
        FlavourCategory dbItem = flavourCategoryRepository.findById(flavourCategory.getId());
        if(dbItem != null){
            dbItem.setName(flavourCategory.getName());
            dbItem.setDescription(flavourCategory.getDescription());
            dbItem.setImage(flavourCategory.getImage());
            if(flavourCategory.getFlavours().size() > 0){
                dbItem.setFlavours(flavourCategory.getFlavours());
            }
        }
    }

    @Override
    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void delete(int id) {
        FlavourCategory dbItem = flavourCategoryRepository.findById(id);
        if(dbItem != null){
            flavourCategoryRepository.delete(dbItem);
        }

    }

    @Override
    public FlavourCategory findById(int id) {
        return flavourCategoryRepository.findById(id);
    }

    @Override
    public List<FlavourCategory> findAll() {
        return flavourCategoryRepository.findAll();
    }
}
