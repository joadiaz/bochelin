package com.bochelin.services;

import com.bochelin.domain.*;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

    private static final Logger logger = Logger.getLogger(ReportServiceImpl.class);

    @Autowired
    BillService billService;

    public Report getMonthlyReport(int monthNumber) {
        Report report = new Report();
        float total = 0.0f;
        report.setDailySellList(this.generateDailySells(billService.getMonthlyBills(monthNumber)));

        for (int i = 0; i < report.getDailySellList().size(); i++) {
            total += report.getDailySellList().get(i).getQuantity();
        }

        report.setTotal(total);
        return report;
    }

    private List<DailySell> generateDailySells(List<Bill> billList){
        List<DailySell> dailySells = new ArrayList<DailySell>();
        logger.debug(billList.size());
        for (int i = 0; i < billList.size(); i++) {
            logger.debug("GENERATING DAILY SELLS");
            Boolean added = false;
            for (int j = 0; j < dailySells.size() && !added; j++) {
                if (DateUtils.isSameDay(billList.get(i).getBillDate(), dailySells.get(j).getSellDate())) {
                    added = true;
                    dailySells.get(j).setQuantity(dailySells.get(j).getQuantity() + billList.get(i).getTotal());
                    logger.debug("GENERATING PRODUCT LIST");
                    dailySells.get(j).addProductsToList(billList.get(i).getBillDetails());
                }
            }

            if(!added){
                DailySell newDailySell = new DailySell();
                newDailySell.setSellDate(DateUtils.truncate(billList.get(i).getBillDate(), Calendar.DATE));
                newDailySell.setQuantity(billList.get(i).getTotal());
                logger.debug("GENERATING PRODUCT LIST");
                newDailySell.addProductsToList(billList.get(i).getBillDetails());
                dailySells.add(newDailySell);
            }
        }
        return dailySells;
    }
}
