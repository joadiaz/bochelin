package com.bochelin.services;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {

    public String uploadFile(MultipartFile file);

}
