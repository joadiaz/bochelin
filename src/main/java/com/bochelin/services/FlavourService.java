package com.bochelin.services;

import com.bochelin.domain.Flavour;

import java.util.List;

public interface FlavourService extends ABMService<Flavour> {

    public List<Flavour> search(String filter);

    public List<Flavour> searchByCategory(int categoryId, String filter);

    public Boolean flavourHasStock(int flavourId, Float quantity);

    public void reduceFlavourStock(Flavour flavour, float weightPerFlavour);

    public void updateStock(Flavour flavour);
}
