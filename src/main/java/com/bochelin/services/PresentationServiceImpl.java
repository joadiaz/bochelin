package com.bochelin.services;

import com.bochelin.domain.Presentation;
import com.bochelin.repositories.PresentationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("presentationService")
@Transactional
public class PresentationServiceImpl implements PresentationService {

    @Autowired
    PresentationRepository presentationRepository;

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void save(Presentation presentation) {
        presentationRepository.saveEntity(presentation);
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void update(Presentation presentation) {
        Presentation dbItem = presentationRepository.findById(presentation.getId());
        if(dbItem != null){
            dbItem.setName(presentation.getName());
            dbItem.setDescription(presentation.getDescription());
            dbItem.setImage(presentation.getImage());
            dbItem.setMaxFlavours(presentation.getMaxFlavours());
            dbItem.setCustomizable(presentation.isCustomizable());
            dbItem.setPrice(presentation.getPrice());
            dbItem.setWeight(presentation.getWeight());
            if(presentation.getProducts().size() > 0){
                dbItem.setProducts(presentation.getProducts());
            }
        }
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_MANAGER')")
    public void delete(int id) {
        Presentation dbItem = presentationRepository.findById(id);
        if(dbItem != null){
            presentationRepository.deleteEntity(dbItem);
        }
    }

    public Presentation findById(int id) {
        return presentationRepository.findById(id);
    }

    public List<Presentation> findAll() {
        return presentationRepository.findAll();
    }

    public float getWeightPerFlavour(int id, int flavourQuantity){
        return presentationRepository.findById(id).getWeight() / flavourQuantity;
    }
}
