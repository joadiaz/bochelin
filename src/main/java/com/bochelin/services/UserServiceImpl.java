package com.bochelin.services;

import com.bochelin.common.Profile;
import com.bochelin.domain.User;
import com.bochelin.domain.UserProfile;
import com.bochelin.repositories.UserRepository;
import com.bochelin.utils.PasswordManagment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserProfileService userProfileService;

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User findByEmail(String email) { return userRepository.findByEmail(email); }

    public void saveUser(User user) {

        //Getting default user profile -> USER
        UserProfile defaultUserProfile = userProfileService.findByType(Profile.CLIENT.toString());

        //Hashing user password using Bcrypt
        user.setPassword(PasswordManagment.hashPassword(user.getPassword()));
        user.setUserProfile(defaultUserProfile);
        logger.debug(user.getUsername());
        userRepository.saveUser(user);
    }

    public void updateUser(User user) {
        User userToUpdate = userRepository.findByUsername(user.getUsername());
        if(userToUpdate != null){
            userToUpdate.setPassword(user.getPassword());
        }
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_ADMIN')")
    public void deleteUser(User user) {
        userRepository.deleteUser(user);
    }

    @PreAuthorize(value="isAuthenticated() and hasRole('ROLE_ADMIN')")
    public List<User> findAllUsers() {
        return userRepository.findAllUsers();
    }
}
