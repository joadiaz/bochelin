package com.bochelin.dao;

import com.bochelin.domain.BillDetail;

public interface BillDetailsDAO extends ABMDAO<BillDetail> {
}
