package com.bochelin.dao;

import com.bochelin.domain.Presentation;

public interface PresentationDAO extends ABMDAO<Presentation> {
}
