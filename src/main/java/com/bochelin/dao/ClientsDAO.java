package com.bochelin.dao;

import com.bochelin.domain.Client;

public interface ClientsDAO extends ABMDAO<Client> {
}
