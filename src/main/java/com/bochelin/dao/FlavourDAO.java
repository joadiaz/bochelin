package com.bochelin.dao;

import com.bochelin.domain.Flavour;

public interface FlavourDAO extends ABMDAO<Flavour>{
}
