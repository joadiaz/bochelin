package com.bochelin.dao;

import java.io.Serializable;

import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract  class AbstractDAO<PK extends Serializable, T> {

    private static final Logger logger = Logger.getLogger(AbstractDAO.class);

    private final Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession(){
        logger.debug("Getting current session");
        Session session = sessionFactory.getCurrentSession();
        logger.debug("Current session = " + session.toString());
        logger.debug("Is open ? = " + session.isOpen());
        return session;
    }

    @SuppressWarnings("unchecked")
    public T getByKey(PK key) {
        return (T) getSession().get(persistentClass, key);
    }

    public void persist(T entity) {
        getSession().persist(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    protected Criteria createEntityCriteria(){
        return getSession().createCriteria(persistentClass);
    }

}