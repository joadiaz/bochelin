package com.bochelin.dao;

import com.bochelin.domain.Bill;

public interface BillsDAO extends ABMDAO<Bill> {

}
