package com.bochelin.dao;

import com.bochelin.domain.UserProfile;

import java.util.List;

public interface UserProfileDAO {

    List<UserProfile> findAll();

    UserProfile findByType(String type);

    UserProfile findById(int id);
}
