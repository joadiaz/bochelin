package com.bochelin.dao;

import java.util.List;

public interface ABMDAO<T> {

    void saveEntity(T entity);

    void deleteEntity(T entity);

    List<T> findAll();

    T findById(int id);

}
