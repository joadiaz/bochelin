package com.bochelin.dao;

import com.bochelin.domain.FlavourCategory;

public interface FlavourCategoryDAO extends ABMDAO<FlavourCategory> {

}
