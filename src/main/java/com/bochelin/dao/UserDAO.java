package com.bochelin.dao;

import com.bochelin.domain.User;

import java.util.List;

public interface UserDAO {

    User findByUsername(String username);

    void saveUser(User user);

    void deleteUser(User user);

    List<User> findAllUsers();

    User findByEmail(String email);

}
