package com.bochelin.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Configuration
@ComponentScan("com.bochelin.controllers")
public class WebApp implements WebApplicationInitializer {


    public void onStartup( ServletContext servletContext ) throws ServletException {
        FilterRegistration.Dynamic fr = servletContext.addFilter("JWTFilter", JWTFilter.class);
        fr.addMappingForUrlPatterns(null, true, "/api/*");
    }

}
