package com.bochelin.configuration;

import com.bochelin.common.exceptions.*;
import com.bochelin.utils.ValidationErrorBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice(basePackages = {"com.bochelin.controllers"} )
public class GlobalDefaultExceptionHandler {

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseBody
    public GenericErrorMessage userNotAuthorized(AccessDeniedException e) {
        UserNotAuthorizedException ex = new UserNotAuthorizedException();
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

    @ExceptionHandler(BadUserOrPasswordException.class)
    @ResponseBody
    public GenericErrorMessage badUserOrPassword(BadUserOrPasswordException e) {
        BadUserOrPasswordException ex = new BadUserOrPasswordException();
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public GenericErrorMessage handleMissingFieldsException(MethodArgumentNotValidException exception) {
        MissingFieldsException ex = new MissingFieldsException(createValidationError(exception).getErrors());
        return new GenericErrorMessage(ex.getCode(), ex.getExceptionMessage(), ex.getErrorDetails());
    }

    private ValidationErrorMessage createValidationError(MethodArgumentNotValidException e) {
        return ValidationErrorBuilder.fromBindingErrors(e.getBindingResult());
    }

}
