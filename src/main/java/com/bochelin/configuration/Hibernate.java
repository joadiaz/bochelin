package com.bochelin.configuration;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.bochelin.configuration" })
@PropertySource(value = { "classpath:application.properties" })
public class Hibernate {

    private static final Logger logger = Logger.getLogger(Hibernate.class);

    @Autowired
    private Environment environment;

    @Bean
    public SessionFactory sessionFactory() {
        logger.debug("Getting current session origin");
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource());
        sessionBuilder.scanPackages("com.bochelin.domain");
        sessionBuilder.addProperties(hibernateProperties());
        return sessionBuilder.buildSessionFactory();
    }

    @Bean
    public DataSource dataSource() {
        String systemEnvironment = this.getSystemEnvironment();
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc."+systemEnvironment+".driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc."+systemEnvironment+".url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc."+systemEnvironment+".username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc."+systemEnvironment+".password"));
        return dataSource;
    }

    private Properties hibernateProperties() {
        String systemEnvironment = this.getSystemEnvironment();
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate."+systemEnvironment+".dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
        properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
        return properties;
    }

    private String getSystemEnvironment() {
        String systemEnvironment = System.getenv("env");
        if (systemEnvironment == null) {
            systemEnvironment = "production";
        }
        return systemEnvironment;
    }

    @Autowired
    @Bean
    public HibernateTransactionManager getTransactionManager(
            SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager(
                sessionFactory);

        return transactionManager;
    }

}
