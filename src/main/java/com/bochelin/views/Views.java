package com.bochelin.views;

public class Views {
    public interface Summary {}
    public interface FlavourSummary extends Summary {}
    public interface ProductSumary extends Summary {}
    public interface PresentationSummary extends ProductSumary {}
}
