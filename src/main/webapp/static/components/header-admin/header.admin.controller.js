angular.module('bochelin')
    .controller('HeaderAdminCtrl', function($scope, Session, $state) {
        console.log('Header Admin Loaded');
        $scope.logout = function(){
            Session.destroy();
            $state.go('base');
        }
    });
