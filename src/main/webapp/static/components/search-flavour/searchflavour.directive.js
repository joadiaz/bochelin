angular.module('bochelin')
	.directive('searchFlavour', searchFlavour);

function searchFlavour(){

	return {
		restrict: 'EA',
		templateUrl: 'components/search-flavour/searchflavour.html',
		controller: SearchCtrl,
		controllerAs: 'vm',
		bindToController: true,
		scope: {
			maxFlavours: '@',
			flavours: '='
		}
	}

	SearchCtrl.$inject = ['$scope', 'FlavoursService', 'CategoriesService', 'MessagesService'];

	function SearchCtrl($scope, FlavoursService, CategoriesService, MessagesService){

		var vm = this;
		vm.filter = {
			id: 0
		}

		vm.maxFlavours = parseInt(vm.maxFlavours);

		vm.categories = [];

        getAllCategories();

	    vm.search = function (description){
	        if(description != ""){
	            if(vm.filter.id != '0'){
	                FlavoursService.searchByCategory({id: 'search', filter: vm.filter.id, filter2: description}).$promise.then(function(result){
	                    vm.search.results = result;
	                });
	            }else{
	                FlavoursService.search({id: 'search', filter: description}).$promise.then(function(result){
	                    vm.search.results = result;
	                });
	            }
	        }else{
	            vm.search.results = [];
	        }
	    }

        function getAllCategories(){
            CategoriesService.getAll().$promise.then(function(result) {
                vm.categories = result;
				vm.categories.push({
					id: 0,
					name: 'Todos'
				})
            }, function(error){
                console.log(error);
            })
        }

		vm.addFlavour = function(flavour){
			vm.search.results = [];
			vm.search.description = '';
			if(vm.maxFlavours == 0 || vm.maxFlavours == undefined || vm.maxFlavours < 0){
				MessagesService.errorMessage('Seleccione una presentación');
			}else{
				if(vm.flavours.length < parseInt(vm.maxFlavours) && !vm.isAdded(flavour)){
					vm.flavours.push(flavour);
				}else{
					MessagesService.errorMessage('Llegaste al máximo de sabores');
				}
			}
		}

		vm.isAdded = function (flavour){
			var found = false;
			vm.flavours.forEach(function(addedFlavour){
				if (addedFlavour.id == flavour.id){
					found = true;
				}
			})
			return found;
		}

	}
}
