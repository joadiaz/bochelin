angular.module('bochelin')
	.directive('fileUpload', fileUpload);

function fileUpload(){

	return {
		restrict: 'EA',
		templateUrl: 'components/file-upload/fileupload.html',
		controller: fileUploadCtrl,
		controllerAs: 'vm',
		bindToController: true,
		scope: {
			imagePath: '='
		}
	}

	fileUploadCtrl.$inject = ['$scope', 'UploadFactory'];

	function fileUploadCtrl($scope, UploadFactory){
		var vm = this;
        vm.uploader = UploadFactory.uploader;

        vm.uploader.onBeforeUploadItem = function(item) {
            vm.fileupload = item._file.name;
        };

        vm.uploader.onSuccessItem = function(fileItem, response, status, headers) {
            vm.imagePath = response.path;
        };

	}

}
