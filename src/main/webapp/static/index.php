<html ng-app="bochelin">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <base href="/">

        <title>Bochelin</title>

        <!-- bower:css -->
        <link rel='stylesheet' href='/bower_components/bootstrap/dist/css/bootstrap.css' />
        <link rel='stylesheet' href='/bower_components/angular-loading-bar/build/loading-bar.css' />
        <link rel='stylesheet' href='/bower_components/angular-dialog-service/dist/dialogs.min.css' />
        <link rel='stylesheet' href='/bower_components/components-font-awesome/css/font-awesome.css' />
        <link rel='stylesheet' href='/bower_components/angular-carousel/dist/angular-carousel.css' />
        <link rel='stylesheet' href='/bower_components/angular-material/angular-material.css' />
        <link rel='stylesheet' href='/bower_components/angular-material-data-table/dist/md-data-table.css' />
        <!-- endbower -->

        <!-- start stylesheets tags -->
        <link rel="stylesheet" href="/app.css"/>
        <!-- end stylesheets tags -->

        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic' rel='stylesheet' type='text/css'>

    </head>
    <body >
        <div ui-view="header"></div>
        <div ui-view></div>
        <div ui-view="footer"></div>

        <!-- bower:js -->
        <script src="/bower_components/jquery/dist/jquery.js"></script>
        <script src="/bower_components/angular/angular.js"></script>
        <script src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>
        <script src="/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
        <script src="/bower_components/angular-resource/angular-resource.js"></script>
        <script src="/bower_components/angular-loading-bar/build/loading-bar.js"></script>
        <script src="/bower_components/lodash/lodash.js"></script>
        <script src="/bower_components/angular-google-maps/dist/angular-google-maps.js"></script>
        <script src="/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
        <script src="/bower_components/angular-animate/angular-animate.js"></script>
        <script src="/bower_components/angular-file-upload/dist/angular-file-upload.min.js"></script>
        <script src="/bower_components/angular-cookies/angular-cookies.js"></script>
        <script src="/bower_components/angular-sanitize/angular-sanitize.js"></script>
        <script src="/bower_components/angular-translate/angular-translate.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs.min.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs-default-translations.min.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs-default-translations.js"></script>
        <script src="/bower_components/angular-smart-table/dist/smart-table.js"></script>
        <script src="/bower_components/angular-touch/angular-touch.js"></script>
        <script src="/bower_components/angular-carousel/dist/angular-carousel.js"></script>
        <script src="/bower_components/angular-aria/angular-aria.js"></script>
        <script src="/bower_components/angular-messages/angular-messages.js"></script>
        <script src="/bower_components/angular-material/angular-material.js"></script>
        <script src="/bower_components/angular-material-data-table/dist/md-data-table.js"></script>
        <!-- endbower -->


        <!-- start javascripts tags -->
        <script type="text/javascript" src="/app/app.js"></script>
        <script type="text/javascript" src="/app/admin/flavours/detail/detail.controller.js"></script>
        <script type="text/javascript" src="/app/admin/flavours/flavours.controller.js"></script>
        <script type="text/javascript" src="/app/admin/flavours/flavours.service.js"></script>
        <script type="text/javascript" src="/app/admin/home/home.controller.js"></script>
        <script type="text/javascript" src="/app/admin/packages/detail/detail.controller.js"></script>
        <script type="text/javascript" src="/app/admin/packages/packages.controller.js"></script>
        <script type="text/javascript" src="/app/admin/packages/packages.service.js"></script>
        <script type="text/javascript" src="/app/common/auth.service.js"></script>
        <script type="text/javascript" src="/app/common/headerauth.factory.js"></script>
        <script type="text/javascript" src="/app/common/session.service.js"></script>
        <script type="text/javascript" src="/app/common/upload.factory.js"></script>
        <script type="text/javascript" src="/app/home/home.controller.bk.js"></script>
        <script type="text/javascript" src="/app/home/home.controller.js"></script>
        <script type="text/javascript" src="/app/login/login.controller.js"></script>
        <script type="text/javascript" src="/app/routes.js"></script>
        <script type="text/javascript" src="/app/shop/shop.controller.js"></script>
        <script type="text/javascript" src="/app/shop/shop.service.js"></script>
        <script type="text/javascript" src="/components/admin-menu/admin.menu.controller.js"></script>
        <script type="text/javascript" src="/components/backend-slider/slider.controller.js"></script>
        <script type="text/javascript" src="/components/footer/footer.controller.js"></script>
        <script type="text/javascript" src="/components/header-admin/header.admin.controller.js"></script>
        <script type="text/javascript" src="/components/header/header.controller.js"></script>
        <script type="text/javascript" src="/components/search-flavour/searchflavour.directive.js"></script>
        <script type="text/javascript" src="/components/slider/slider.controller.js"></script>
        <!-- end javascripts tags -->

    </body>
</html>