'use strict';

module.exports = function (grunt) {
  // Load grunt tasks automatically, when needed
  require('jit-grunt')(grunt, {
    sass : 'grunt-contrib-sass',
    wiredep : 'grunt-wiredep',
    tags : 'grunt-script-link-tags',
    watch : 'grunt-contrib-watch',
    injector: 'grunt-injector'
  });


  grunt.initConfig({
    sass : {
      dist : {
        files : {
          'app/app.css' : 'app/app.sass'
        },
        options : {
          sourcemap : 'none'
        }
      }
    },
    injector: {
        options: {},
        bower_dependencies: {
          files: {
            '../WEB-INF/views/index.jsp': [
                'bower.json',
                'app/app.js',
                'app/**/*.js',
                'components/**/*.js',
                'app/app.css'
                ],
          }
        }
    },
    watch : {
      express : {
        files : [
          'app/**/*.js',
          'app/**/*.sass',
          'components/**/*.js',
          'components/**/*.sass',
          '*.js',
          '*.json'
        ],
        tasks : [
          'sass:dist',
          'injector',
        ],
        options : {
          event : 'all',
          spawn : false
        }
      }
    }
  });

  grunt.registerTask('default', [
    'sass:dist',
    'injector',
    'watch'
  ]);
};
