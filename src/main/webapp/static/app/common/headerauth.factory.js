angular.module('bochelin')
    .factory('httpRequestInterceptor', function (Session) {
        return {
            request: function (config) {

                // use this to destroying other existing headers
                config.headers = {'Authorization': 'Bearer ' + Session.getToken(), 'Content-Type':'application/json' }

                // use this to prevent destroying other existing headers
                // config.headers['Authorization'] = 'authentication;

                return config;
            }
        };
    });
