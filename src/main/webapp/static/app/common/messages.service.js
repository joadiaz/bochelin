angular.module('bochelin')
    .service('MessagesService', function ($mdToast, MESSAGES, $mdDialog, $sce) {

        var toastDuration = 2000;
        var successTheme = 'success-toast';
        var errorTheme = 'error-toast';

        this.invalidForm = function () {
            $mdToast.show(
              $mdToast.simple()
                .textContent(MESSAGES.invalidForm)
                .position('top right')
                .hideDelay(toastDuration)
                .theme(errorTheme)
            );
        };

        this.successMessage = function (message){
            $mdToast.show(
              $mdToast.simple()
                .textContent($sce.trustAsHtml(message))
                .position('top right')
                .hideDelay(toastDuration)
                .theme(successTheme)
            );
        }

        this.confirmDelete = function (ev, title, message){
            var confirm = $mdDialog.confirm()
                  .title(title)
                  .textContent(message)
                  .ariaLabel('Eliminar')
                  .targetEvent(ev)
                  .ok(MESSAGES.acceptDelete)
                  .cancel(MESSAGES.deniedDelete);
            return $mdDialog.show(confirm);
        }

        this.errorMessage = function(message){
            $mdToast.show(
              $mdToast.simple()
                .textContent(message)
                .position('top right')
                .hideDelay(toastDuration)
                .theme(errorTheme)
            );
        }

    });
