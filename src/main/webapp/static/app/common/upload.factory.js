angular.module('bochelin')
	.factory('UploadFactory', uploadFactory);

function uploadFactory(FileUploader, SERVER, $cookies){
	
	var uploader = new FileUploader({
            url: SERVER.address + '/upload',
			headers: {
				'X-XSRF-TOKEN': $cookies.get('XSRF-TOKEN')
			}
        });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onAfterAddingFile = function (item) {
        item.upload();
    };

    return {
        uploader: uploader
    }

}
