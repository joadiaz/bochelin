angular.module('bochelin')
    .factory('AuthService', function($resource, SERVER) {
        return $resource(SERVER.address + '/login', {
            },
            {
                login : {
                    method : 'POST'
                }
            });
    });