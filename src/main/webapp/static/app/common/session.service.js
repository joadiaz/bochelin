angular.module('bochelin')
    .service('Session', function ($cookieStore) {
        this.create = function (authToken, role) {
            $cookieStore.put('token', authToken);
            $cookieStore.put('role', role);
        };
        this.isAuthenticated = function(){
            if($cookieStore.get('token')) return true;
            return false;
        };
        this.getToken = function(){
            return $cookieStore.get('token');
        };
        this.getRole = function(){
            return $cookieStore.get('role');
        }
        this.destroy = function(){
            $cookieStore.put('token', null);
            $cookieStore.put('role', null);
        }
    });
