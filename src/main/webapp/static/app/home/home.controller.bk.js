angular.module('bochelin')
    .controller('HomeCtrlBk', function($scope, PackageService, FlavoursService, SellsService) {
        console.log('Home controller loaded');

        $scope.packages = null;
        $scope.categories = null;

        $scope.filter = {
            category : null
        }

        $scope.cart = {
            client : {
                firstName : '',
                lastName : '',
                telephone : '',
                mail : '',
                address : ''
            },
            products : [],
            total : ''
        }

        $scope.newProduct = {
            packageId : '',
            description : '',
            isCustom : false,
            flavours : [],
            price : '',
            quantity : 1
        }

        $scope.error = {
            message : '',
            show : false
        }

        $scope.selectedProduct = null;

        getAllPackages();
        getAllCategories();

        $scope.steps = [true, false, false]

        function getAllCategories(){
            FlavoursService.getAll().$promise.then(function(result){
                $scope.categories = result;
                $scope.categories.push({
                    description : 'Buscar por categoría',
                    id : 0
                });
                $scope.filter.category = $scope.categories[$scope.categories.length-1];
            })
        }

        function getAllPackages(){
            PackageService.getAll().$promise.then(function(result) {
                $scope.packages = result;
            }, function(error){
                console.log(error);
            })
        }

        $scope.selectPackage = function(package){
            console.log($scope.packages);
            $scope.package = package;
            $scope.steps[0] = false;
            $scope.steps[1] = true;
            $scope.newProduct.packageId = package.id;
        }

        $scope.search = function (description){
            if(description != ""){
                if($scope.filter.category.id != '0'){
                    FlavoursService.search({id: 'search', description: description, categoryDescription: $scope.filter.category.description}).$promise.then(function(result){
                        $scope.search.results = result;
                    });
                }else{
                    FlavoursService.search({id: 'search', description: description}).$promise.then(function(result){
                        $scope.search.results = result;
                    });
                }
            }else{
                $scope.search.results = [];
            }
        }

        $scope.back = function(){
            $scope.package.products.forEach(function(item){
                item.active = false;
            })

            $scope.newProduct = {
                packageId : '',
                description : '',
                isCustom : false,
                flavours : [],
                price : '',
                quantity : 1
            }

            $scope.error = {
                show : false,
            }

            for (i=0; i < $scope.steps.length; i++){
                if ($scope.steps[i] == true){
                    $scope.steps[i] = false;
                    $scope.steps[i-1] = true;
                }
            }
        }

        $scope.addElement = function(flavour){
            $scope.newProduct.isCustom = true;

            $scope.package.products.forEach(function(item){
                item.active = false;
            })

            if ($scope.newProduct.flavours.length < $scope.package.maxFlavours && getSelectedFlavourIndex(flavour) == -1){
                $scope.newProduct.flavours.push(flavour);
            }

            $scope.search.description = "";
            $scope.search.results = [];
        }

        function getSelectedFlavourIndex(flavour){
            index = 0;
            winner = -1;
            $scope.newProduct.flavours.forEach(function(item){
                if (item.description == flavour.description){
                    winner = index;
                }
                index++;
            })
            return winner;
        }

        $scope.deleteFlavour = function(index){
            $scope.newProduct.flavours.splice(index, 1);

            if($scope.newProduct.flavours.length == 0){
                $scope.newProduct.isCustom = false;
            }

        }

        $scope.selectProduct = function(product){
            $scope.package.products.forEach(function(item){
                item.active = false;
            })
            product.active = true;
            $scope.selectedProduct = product;
        }

        $scope.buy = function(){

            if ($scope.selectedProduct == null && $scope.package.customizable == false) {
                $scope.error = {
                    show : true,
                    message : 'Seleccione al menos un producto'
                }
                return false;
            }else if($scope.newProduct.flavours.length == 0 && $scope.package.customizable == true && $scope.selectedProduct == null){
                $scope.error = {
                    show : true,
                    message : 'Seleccione al menos un producto o un sabor'
                }
                return false;
            }

            $scope.error = {
                show : false,
            }

            $scope.steps[1] = false;
            $scope.steps[2] = true;

            $scope.newProduct.packageId = $scope.package.id;
            if ($scope.newProduct.isCustom == false){
                $scope.newProduct.description = $scope.selectedProduct.description;
                $scope.newProduct.price = $scope.selectedProduct.price;
                $scope.newProduct.flavours = $scope.selectedProduct.flavours;
            }else{
                $scope.newProduct.price = $scope.package.price;
                $scope.newProduct.description = $scope.package.description;
            }

            $scope.cart.products.push($scope.newProduct);

            console.log($scope.cart);

            $scope.refreshPrice();
        }

        $scope.deleteProduct = function(index){
            $scope.cart.products.splice(index, 1);
        }

        $scope.gotoCart = function(){
            $scope.steps[0] = false;
            $scope.steps[1] = false;
            $scope.steps[2] = true;
            $scope.refreshPrice();
        }

        $scope.refreshPrice = function(){
            $scope.cart.total = 0;
            $scope.cart.products.forEach(function(product){
                $scope.cart.total += product.price*product.quantity;
            })
        }

        $scope.endBuy = function(clientForm){
            if(clientForm.$valid) {
                SellsService.save($scope.cart).$promise.then(function(result){
                    console.log(result)
                })
            }
        }

    });