angular.module('bochelin')
    .controller('HomeCtrl', function($scope) {
        console.log('Home controller loaded');
        
        $scope.slides = [
            {active: false, image:'/images/slider/1.jpg'},
            {active: false, image:'/images/slider/2.jpg'},
            {active: false, image:'/images/slider/3.jpg'},
        ];

    });