angular.module('bochelin')
    .controller('LoginCtrl', function($scope, $state, AuthService, Session) {
        console.log('Login Ctrl Loaded');
        $scope.error = null;

        $scope.login = function (credentials) {
            AuthService.login(JSON.stringify(credentials)).$promise.then(function (results) {
                console.log(results);
                if (results.code) {
                    $scope.error = results.message;
                } else {
                    Session.create(results.token, results.role);
                    $state.go('admin.flavours');    
                }
            }, function (error){

            })
        }

    });
