angular.module('bochelin')
    .controller('ReportsCtrl', function($scope, $state, Session, ReportsService, MessagesService) {
        console.log('Reports controller loaded');
        $scope.userRole = Session.getRole();
        $scope.selectedMonth = 1;
        $scope.report = null;
        $scope.months = [
            {
                number: 1,
                name: "Enero"
            },
            {
                number: 2,
                name: "Febrero"
            },
            {
                number: 3,
                name: "Marzo"
            },
            {
                number: 4,
                name: "Abril"
            },
            {
                number: 5,
                name: "Mayo"
            },
            {
                number: 6,
                name: "Junio"
            },
            {
                number: 7,
                name: "Julio"
            },
            {
                number: 8,
                name: "Agosto"
            },
            {
                number: 9,
                name: "Septiembre"
            },
            {
                number: 10,
                name: "Octubre"
            },
            {
                number: 11,
                name: "Noviembre"
            },
            {
                number: 12,
                name: "Diciembre"
            }
        ];

        $scope.getReport = function () {
            ReportsService.get({month: $scope.selectedMonth}).$promise.then(function(result) {
                $scope.report = result;
                $scope.report.dailySellList.forEach(function(dailySell){
                    var total = 0;
                    dailySell.dailyProductSellList.forEach(function(item){
                        total += item.quantity
                    });
                    dailySell.productTotal = total;
                });
                console.log(result);
            }, function(error){
                console.log(error);
            });
        }

    });
