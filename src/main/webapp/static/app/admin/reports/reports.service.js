angular.module('bochelin')
    .factory('ReportsService', function($resource, SERVER) {
            return $resource(SERVER.address + '/reports/:month', {
                    month : '@month'
            },
            {
                get : {
                    method : 'GET'
                }
            });
    });
