angular.module('bochelin')
    .factory('FlavoursService', function($resource, SERVER) {
            return $resource(SERVER.address + '/flavours/:id/:filter/:filter2', {
                    id : '@id',
                    filter: '@filter',
                    filter2: '@filter2'
            },
            {
                save : {
                    method : 'POST'
                },
                update : {
                    method : 'PUT'
                },
                delete : {
                    method : 'DELETE'
                },
                getAll : {
                    method : 'GET',
                    isArray : true
                },
                getOne : {
                    method : 'GET'
                },
                search : {
                    method : 'GET',
                    isArray : true,
                },
                searchByCategory : {
                    method : 'GET',
                    isArray : true,
                },
                updateStock: {
                    method: 'POST',
                    url: SERVER.address + '/flavours/update-stock'
                }
            });
    });
