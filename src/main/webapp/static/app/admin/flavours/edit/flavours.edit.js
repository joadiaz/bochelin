angular.module('bochelin')
    .controller('FlavoursEditCtrl', function($scope, $state, Session, CategoriesService, MESSAGES, $stateParams, MessagesService, FlavoursService) {
        console.log('Flavours edit controller loaded');

        var actions = {
            create: 0,
            edit: 1
        };

        var currentAction = actions.create;

        $scope.action = 'Crear';
        $scope.role = Session.getRole();
        $scope.messages = MESSAGES;

        $scope.flavour = {
            name: '',
            description: '',
            stock: 0.0,
            flavourCategory: {
                id: null
            }
        };

        $scope.categories = [];

        CategoriesService.getAll().$promise.then(function(result){
            $scope.categories = result;
        })

        if ($stateParams.id != 'new'){
            FlavoursService.getOne({id:$stateParams.id}).$promise.then(function(result){
                if (result != null){
                    $scope.flavour = result;
                    currentAction = actions.edit;
                    $scope.action = 'Editar';
                }
            })
        }

        $scope.maxlenght = {
            name: 100,
            description: 255
        };
        $scope.minlenght = {
            name: 6,
            description: 10
        };
        $scope.saveFlavour = function(){
            if(!$scope.editFlavourForm.$invalid){
                if ($scope.role == 'MANAGER') {
                    if(currentAction == actions.create){
                        FlavoursService.save(JSON.stringify($scope.flavour)).$promise.then(function(result){
                            MessagesService.successMessage('Sabor creado con éxito');
                            $state.go('admin.flavours');
                        });
                    }else{
                        FlavoursService.update(JSON.stringify($scope.flavour)).$promise.then(function(){
                            MessagesService.successMessage('Sabor editado con éxito');
                            $state.go('admin.flavours');
                        });
                    }
                } else {
                    FlavoursService.updateStock(JSON.stringify($scope.flavour)).$promise.then(function(){
                        MessagesService.successMessage('Sabor editado con éxito');
                        $state.go('admin.flavours');
                    });
                }
            }else{
                MessagesService.invalidForm();
            }
        }
    });
