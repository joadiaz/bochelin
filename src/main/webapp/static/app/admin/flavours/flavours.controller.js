angular.module('bochelin')
    .controller('FlavoursCtrl', function($scope, $state, Session, FlavoursService, MessagesService) {
        console.log('Flavour controller loaded');
        $scope.userRole = Session.getRole();
        $scope.flavours = [];

        FlavoursService.getAll().$promise.then(function(result) {
            $scope.flavours = result;
        }, function(error){
            console.log(error);
        });

        $scope.deleteFlavour = function($event, flavour, $index){
            MessagesService.confirmDelete($event, '¿Desea borrar el sabor', 'El sabor se eliminará permanentemente.')
                           .then(function(){
                               FlavoursService.delete({id: flavour.id}).$promise.then(function(){
                                   MessagesService.successMessage('Sabor borrado con éxito');
                                   $scope.flavours.splice($index, 1);
                               });
                           }, function(){

                           })
        };
    });
