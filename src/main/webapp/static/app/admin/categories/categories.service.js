angular.module('bochelin')
    .factory('CategoriesService', function($resource, SERVER) {
            return $resource(SERVER.address + '/flavours/category/:id', {
                    id : '@id'
            },
            {
                save : {
                    method : 'POST'
                },
                update : {
                    method : 'PUT'
                },
                delete : {
                    method : 'DELETE'
                },
                getAll : {
                    method: 'GET',
                    isArray: true
                },
                getOne : {
                    method : 'GET'
                }
            });
    });
