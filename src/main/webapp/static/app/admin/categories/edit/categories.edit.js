angular.module('bochelin')
    .controller('CategoriesEditCtrl', function($scope, $state, Session, CategoriesService, MESSAGES, $stateParams, MessagesService) {
        console.log('Categories edit controller loaded');

        var actions = {
            create: 0,
            edit: 1
        };

        var currentAction = actions.create;

        $scope.action = 'Crear';
        $scope.userRole = Session.getRole();
        $scope.messages = MESSAGES;

        $scope.category = {
            name: '',
            description: ''
        };


        if ($stateParams.id != 'new'){
            CategoriesService.getOne({id:$stateParams.id}).$promise.then(function(result){
                if (result != null){
                    $scope.category = result;
                    currentAction = actions.edit;
                    $scope.action = 'Editar';
                }
            })
        }

        $scope.maxlenght = {
            name: 100,
            description: 255
        };
        $scope.minlenght = {
            name: 6,
            description: 10
        };
        $scope.saveCategory = function(){
            if(!$scope.editCategoryForm.$invalid){
                if(currentAction == actions.create){
                    CategoriesService.save(JSON.stringify($scope.category)).$promise.then(function(result){
                        MessagesService.successMessage('Categoría creada con éxito');
                        $state.go('admin.categories');
                    });
                }else{
                    CategoriesService.update(JSON.stringify($scope.category)).$promise.then(function(){
                        MessagesService.successMessage('Categoría editada con éxito');
                        $state.go('admin.categories');
                    });
                }

            }else{
                MessagesService.invalidForm();
            }
        }
    });
