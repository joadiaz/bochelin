angular.module('bochelin')
    .controller('CategoriesCtrl', function($scope, $state, Session, CategoriesService, MessagesService) {
        console.log('Categories controller loaded');
        $scope.userRole = Session.getRole();
        $scope.categories = [];

        CategoriesService.getAll().$promise.then(function(result) {
            $scope.categories = result;
        }, function(error){
            console.log(error);
        });

        $scope.deleteCategory = function($event, category, $index){
            MessagesService.confirmDelete($event, '¿Desea borrar la categoría?', 'La categoría se eliminará permanentemente.')
                           .then(function(){
                               CategoriesService.delete({id: category.id}).$promise.then(function(){
                                   MessagesService.successMessage('Categoría borrada con éxito');
                                   $scope.categories.splice($index, 1);
                               });
                           }, function(){

                           })
        };
    });
