angular.module('bochelin')
    .controller('ProductsEditCtrl', function($scope, $state, Session, MESSAGES,
        $stateParams, MessagesService, ProductsService, PresentationsService, FlavoursService) {
        console.log('Products edit controller loaded');

        var actions = {
            create: 0,
            edit: 1
        };

        var currentAction = actions.create;

        $scope.action = 'Crear';
        $scope.role = Session.getRole();
        $scope.messages = MESSAGES;

        $scope.presentations = [];

        PresentationsService.getAll().$promise.then(function(result){
            $scope.presentations = result;
        });

        $scope.product = {
            name: '',
            description: '',
            price: 0.0,
            stock: 0,
            presentation: {
                id: null
            },
            flavours: []
        };

        if ($stateParams.id != 'new'){
            ProductsService.getOne({id:$stateParams.id}).$promise.then(function(result){
                if (result != null){
                    $scope.product = result;
                    currentAction = actions.edit;
                    $scope.action = 'Editar';
                }
            })
        }

        $scope.maxlenght = {
            name: 100,
            description: 255
        };
        $scope.minlenght = {
            name: 6,
            description: 10
        };
        $scope.saveProduct = function(){
            if(!$scope.editProductForm.$invalid){
                if ($scope.role == 'MANAGER') {
                    dirtyPresentation = $scope.product.presentation;
                    $scope.product.presentation = {
                        id: dirtyPresentation.id
                    };
                    if(currentAction == actions.create){
                        ProductsService.save(JSON.stringify($scope.product)).$promise.then(function(result){
                            MessagesService.successMessage('Producto creado con éxito');
                            $state.go('admin.products');
                        });
                    }else{
                        ProductsService.update(JSON.stringify($scope.product)).$promise.then(function(){
                            MessagesService.successMessage('Producto editado con éxito');
                            $state.go('admin.products');
                        });
                    }
                } else {
                    ProductsService.updateStock(JSON.stringify($scope.product)).$promise.then(function(){
                        MessagesService.successMessage('Producto editado con éxito');
                        $state.go('admin.products');
                    });
                }
            }else{
                MessagesService.invalidForm();
            }
        }

        $scope.removeFlavour = function($index){
            $scope.product.flavours.splice($index, 1);
        }

    });
