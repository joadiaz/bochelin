angular.module('bochelin')
    .factory('ProductsService', function($resource, SERVER) {
            return $resource(SERVER.address + '/products/:id', {
                    id : '@id'
            },
            {
                save : {
                    method : 'POST'
                },
                update : {
                    method : 'PUT'
                },
                delete : {
                    method : 'DELETE'
                },
                getAll : {
                    method : 'GET',
                    isArray : true
                },
                getOne : {
                    method : 'GET'
                },
                updateStock: {
                    method: 'POST',
                    url: SERVER.address + '/products/update-stock'
                }
            });
    });
