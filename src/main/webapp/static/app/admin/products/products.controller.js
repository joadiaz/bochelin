angular.module('bochelin')
    .controller('ProductsCtrl', function($scope, $state, Session, ProductsService, MessagesService) {
        console.log('Products controller loaded');
        $scope.userRole = Session.getRole();
        $scope.products = [];

        ProductsService.getAll().$promise.then(function(result) {
            $scope.products = result;
        }, function(error){
            console.log(error);
        });

        $scope.deleteProduct = function($event, product, $index){
            MessagesService.confirmDelete($event, '¿Desea borrar el producto?', 'El producto se eliminará permanentemente.')
                           .then(function(){
                               ProductsService.delete({id: product.id}).$promise.then(function(){
                                   MessagesService.successMessage('producto borrado con éxito');
                                   $scope.products.splice($index, 1);
                               });
                           }, function(){

                           })
        };
    });
