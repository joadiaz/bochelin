angular.module('bochelin')
    .controller('PresentationsCtrl', function($scope, $state, Session, PresentationsService, MessagesService) {
        console.log('Presentations controller loaded');
        $scope.userRole = Session.getRole();
        $scope.presentations = [];

        PresentationsService.getAll().$promise.then(function(result) {
            $scope.presentations = result;
        }, function(error){
            console.log(error);
        });

        $scope.deletePresentation = function($event, presentation, $index){
            MessagesService.confirmDelete($event, '¿Desea borrar la presentación?', 'La presentación se eliminará permanentemente.')
                           .then(function(){
                               PresentationsService.delete({id: presentation.id}).$promise.then(function(){
                                   MessagesService.successMessage('Presentación borrada con éxito');
                                   $scope.presentations.splice($index, 1);
                               });
                           }, function(){

                           })
        };
    });
