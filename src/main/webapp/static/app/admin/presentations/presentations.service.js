angular.module('bochelin')
    .factory('PresentationsService', function($resource, SERVER) {
            return $resource(SERVER.address + '/products/presentation/:id', {
                    id : '@id'
            },
            {
                save : {
                    method : 'POST'
                },
                update : {
                    method : 'PUT'
                },
                delete : {
                    method : 'DELETE'
                },
                getAll : {
                    method : 'GET',
                    isArray : true
                },
                getOne : {
                    method : 'GET'
                },
                search : {
                    method : 'GET',
                    isArray : true,
                },
                searchByCategory : {
                    method : 'GET',
                    isArray : true,
                }
            });
    });
