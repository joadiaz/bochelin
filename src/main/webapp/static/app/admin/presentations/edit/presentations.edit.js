angular.module('bochelin')
    .controller('PresentationsEditCtrl', function($scope, $state, Session, MESSAGES, $stateParams, MessagesService, PresentationsService) {
        console.log('Presentations edit controller loaded');

        var actions = {
        create: 0,
            edit: 1
        };

        var currentAction = actions.create;

        $scope.action = 'Crear';
        $scope.userRole = Session.getRole();
        $scope.messages = MESSAGES;

        $scope.presentation = {
            name: '',
            description: '',
            maxFlavours: 0,
            isCustomizable: false,
            price: 0.0,
            weight: 0.0
        };

        if ($stateParams.id != 'new'){
            PresentationsService.getOne({id:$stateParams.id}).$promise.then(function(result){
                if (result != null){
                    $scope.presentation = result;
                    currentAction = actions.edit;
                    $scope.action = 'Editar';
                }
            })
        }

        $scope.maxlenght = {
            name: 100,
            description: 255,
            maxFlavours: 2
        };
        $scope.minlenght = {
            name: 6,
            description: 10,
            maxFlavours: 1
        };
        $scope.savePresentation = function(){
            if(!$scope.editPresentationForm.$invalid){
                if(currentAction == actions.create){
                    PresentationsService.save(JSON.stringify($scope.presentation)).$promise.then(function(result){
                        MessagesService.successMessage('Presentación creada con éxito');
                        $state.go('admin.presentations');
                    });
                }else{
                    PresentationsService.update(JSON.stringify($scope.presentation)).$promise.then(function(){
                        MessagesService.successMessage('Presentación editada con éxito');
                        $state.go('admin.presentations');
                    });
                }

            }else{
                MessagesService.invalidForm();
            }
        }
    });
