angular.module('bochelin')
  .config(function ($stateProvider, PERMISSIONS) {
    $stateProvider
      .state('base', {
        url: '/',
        views: {
          header: {
            templateUrl : 'components/header/header.html',
            controller : 'HeaderCtrl'
          },
          footer: {
            templateUrl : 'components/footer/footer.html',
            controller : 'FooterCtrl'
          },
          '': {
            templateUrl : 'app/home/home.html',
            controller : 'HomeCtrl'
          }
        },
        data: {
          permissions: PERMISSIONS.public
        }
      })
      .state('base.shop', {
        url : 'shop',
        views: {
          '@': {
            templateUrl: '/app/shop/shop.html',
            controller: 'ShopCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.public
        }
      })
      .state('base.shop.item', {
        url : '/:id',
        views: {
          '@': {
            templateUrl: '/app/shop/item/shop.item.html',
            controller: 'ShopItemCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.public
        }
      })
      .state('base.checkout', {
        url : 'checkout',
        views: {
          '@': {
            templateUrl: '/app/checkout/checkout.html',
            controller: 'CheckoutCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.public
        }
      })
      .state('base.login', {
        url : 'login',
        views: {
          '@': {
            templateUrl: '/app/login/login.html',
            controller: 'LoginCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.public
        }
      })
      .state('admin', {
        url: '/admin',
        views: {
          header: {
            templateUrl : 'components/header-admin/header.admin.html',
            controller : 'HeaderAdminCtrl'
          },
          footer: {
            templateUrl : 'components/footer/footer.html',
            controller : 'FooterCtrl'
          },
          '': {
            templateUrl : 'app/admin/home/home.html',
            controller : 'HomeAdminCtrl'
          }
        },
        data: {
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.categories', {
        url : '/categories',
        views: {
          content: {
            templateUrl: '/app/admin/categories/categories.html',
            controller: 'CategoriesCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.editcategories', {
        url : '/categories/edit/:id',
        views: {
          content: {
            templateUrl: '/app/admin/categories/edit/categories.edit.html',
            controller: 'CategoriesEditCtrl',
            parent: 'admin.categories'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.flavours', {
        url : '/flavours',
        views: {
          content: {
            templateUrl: '/app/admin/flavours/flavours.html',
            controller: 'FlavoursCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.editflavours', {
        url : '/flavours/edit/:id',
        views: {
          content: {
            templateUrl: '/app/admin/flavours/edit/flavours.edit.html',
            controller: 'FlavoursEditCtrl',
            parent: 'admin.flavours'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.presentations', {
        url : '/presentations',
        views: {
          content: {
            templateUrl: '/app/admin/presentations/presentations.html',
            controller: 'PresentationsCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.editpresentations', {
        url : '/presentations/edit/:id',
        views: {
          content: {
            templateUrl: '/app/admin/presentations/edit/presentations.edit.html',
            controller: 'PresentationsEditCtrl',
            parent: 'admin.presentations'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.products', {
        url : '/products',
        views: {
          content: {
            templateUrl: '/app/admin/products/products.html',
            controller: 'ProductsCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.editproducts', {
        url : '/products/edit/:id',
        views: {
          content: {
            templateUrl: '/app/admin/products/edit/products.edit.html',
            controller: 'ProductsEditCtrl',
            parent: 'admin.products'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })
      .state('admin.reports', {
        url : '/reports',
        views: {
          content: {
            templateUrl: '/app/admin/reports/reports.html',
            controller: 'ReportsCtrl'
          }
        },
        data:{
          permissions: PERMISSIONS.private
        }
      })

  });
