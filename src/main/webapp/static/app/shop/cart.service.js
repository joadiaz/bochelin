angular.module('bochelin')
    .service('cartService', ['ProductsService', '$cookieStore', function(ProductsService, $cookieStore){
        this.cart = [];
        this.client = {
            name: '',
            address: '',
            phone: '',
            dni: '',
            email: ''
        };

        this.addItem = function(product, presentationId){
            if(product.id != undefined){
                var exist = false;
                this.cart.forEach(function(item){
                    if(item.product.id == product.id){
                        exist = true;
                        if(item.quantity == undefined){
                            item.quantity = 2;
                        } else {
                            item.quantity++;
                        }
                    }
                });
                if(exist == false){
                    this.cart.push({
                        product: product,
                        quantity: 1
                    });
                }
            } else {
                product.isCustom = true;
                product.presentation = {
                    id: presentationId
                }
                this.cart.push({
                    product: product,
                    quantity: 1
                });
            }
            this.saveInCookie();
        }

        this.getCart = function(){
            return this.cart;
        }

        this.removeProduct = function(index){
            this.cart.splice(index, 1);
            this.saveInCookie();
        }

        this.checkStocks = function(callback){
            var noStockProducts = [];
            var promisesResolved = 0;
            var promisesToResolve = this.cart.length;
            this.cart.forEach(function(item){
                if (!item.product.isCustom){
                    ProductsService.getOne({id: item.product.id}).$promise.then(function(result){
                        promisesResolved++;
                        if (result.stock < item.quantity) {
                            noStockProducts.push({
                                code: 'P1',
                                message: result.name + ' no tiene stock'
                            });
                        }
                        if (promisesResolved === promisesToResolve) {
                            callback(noStockProducts);
                        }
                    });
                } else {
                    promisesResolved++;
                    if (promisesResolved === promisesToResolve) {
                        callback(noStockProducts);
                    }
                }
            });
        }

        this.generateBill = function (){
            var bill = {
                billDetails: [],
                client: this.client
            };
            this.cart.forEach(function(item){
                var billDetail = {
                    product: {
                        id: item.product.id,
                        name: item.product.name,
                        isCustom: item.product.isCustom,
                    },
                    quantity: item.quantity
                }
                if (item.product.isCustom){
                    billDetail.product.flavours = item.product.flavours;
                    billDetail.product.presentation = item.product.presentation;
                }
                bill.billDetails.push(billDetail);
            });
            return bill;
        }

        this.saveInCookie = function () {
            $cookieStore.put('cart', this.cart);
            $cookieStore.put('client', this.client);
        }

        this.getFromCookie = function () {
            var cartFromCookie = $cookieStore.get('cart');
            var clientFromCookie = $cookieStore.get('client');
            if (cartFromCookie) {
                this.cart = cartFromCookie;
            }
            if (clientFromCookie) {
                this.client = clientFromCookie;
            }
        }

        this.cleanCart = function () {
            this.cart = [];
            this.saveInCookie();
        }

        this.saveClient = function (client){
            this.client = client;
        }

        this.getClient = function () {
            return this.client;
        }

    }]);
