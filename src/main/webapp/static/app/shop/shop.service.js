angular.module('bochelin')
    .factory('SellsService', function($resource, SERVER) {
        return $resource(SERVER.address + '/sells/:id', {
                id : '@id'
            },
            {
                save : {
                    method : 'POST'
                },
                update : {
                    method : 'PUT'
                },
                delete : {
                    method : 'DELETE'
                },
                getAll : {
                    method : 'GET',
                    isArray : true
                },
                getOne : {
                    method : 'GET'
                }
            });
    });