angular.module('bochelin')
    .controller('ShopItemCtrl', function($scope, $stateParams, PresentationsService, cartService, MessagesService) {
        console.log('Shop controller loaded');
        $scope.presentation = [];
        $scope.newProduct = {
            presentation: null
        };
        PresentationsService.getOne({id: $stateParams.id}).$promise.then(function(presentation){
            $scope.presentation = presentation;
            setNewCustomProduct();
        })

        function setNewCustomProduct(presentation){
            if($scope.presentation.isCustomizable){
                $scope.newProduct.presentation = {
                    id: $scope.presentation.id,
                    weight: $scope.presentation.weight
                }
                $scope.selectedFlavours = [];
            }
        }

        $scope.addToCart = function(product){
            if(product != null){
                if (product.stock > 0){
                    cartService.addItem(product);
                    MessagesService.successMessage('Producto agregado al carrito!');
                }
            } else {
                $scope.newProduct.isCustom = true;
                $scope.newProduct.price = $scope.presentation.price;
                $scope.newProduct.name = $scope.presentation.name;
                $scope.newProduct.flavours = $scope.selectedFlavours.slice();
                cartService.addItem($scope.newProduct, $scope.presentation.id);
                $scope.selectedFlavours = [];
                MessagesService.successMessage('Producto agregado al carrito!');
            }
        }

        $scope.removeFlavour = function($index){
            if($scope.selectedFlavours.length == 1){
                $scope.selectedFlavours = [];
            } else {
                $scope.selectedFlavours.splice($index, 1);
            }
        }

    });
