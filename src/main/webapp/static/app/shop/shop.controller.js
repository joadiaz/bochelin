angular.module('bochelin')
    .controller('ShopCtrl', function($scope, PresentationsService) {
        console.log('Shop controller loaded');

        $scope.products = [];

        PresentationsService.getAll().$promise.then(function(result){
            $scope.products = result;
        })
    });
