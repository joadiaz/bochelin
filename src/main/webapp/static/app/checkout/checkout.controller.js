angular.module('bochelin')
    .controller('CheckoutCtrl', function($scope, cartService, CheckoutService, MESSAGES) {
        $scope.cart = cartService.getCart();
        $scope.total = 0;
        $scope.errorList = [];
        $scope.messages = MESSAGES;
        $scope.isFormValid = false;
        $scope.client = cartService.getClient();
        $scope.success = false;
        updatePrice();

        $scope.deleteProduct = function($index){
            cartService.removeProduct($index);
            $scope.cart = cartService.getCart();
            updatePrice();
            $scope.errorList = [];
        };

        $scope.checkout = function(){
            cartService.checkStocks(function(stockErrors){
                if (stockErrors.length > 0) {
                    $scope.errorList = stockErrors;
                } else {
                    $scope.errorList = [];
                    cartService.saveClient($scope.client);
                    var bill = cartService.generateBill();
                    CheckoutService.doCheckout(bill).$promise.then(function(result){
                        if (result.code) {
                            $scope.errorList.push(result);
                        } else {
                            cartService.cleanCart();
                            $scope.cart = cartService.getCart();
                            $scope.success = true;
                        }
                    })
                }
            });
        };

        $scope.showClientForm = function(){
            $scope.clientForm = true;
        }

        function updatePrice(){
            $scope.total = 0;
            $scope.cart.forEach(function(item){
                $scope.total += item.product.price*item.quantity;
            });
        }
    });
