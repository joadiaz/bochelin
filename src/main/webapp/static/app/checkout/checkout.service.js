angular.module('bochelin')
    .factory('CheckoutService', function($resource, SERVER) {
        return $resource(SERVER.address + '/checkout/:id', {
                id : '@id'
            },
            {
                doCheckout : {
                    method : 'POST'
                },
                update : {
                    method : 'PUT'
                },
                delete : {
                    method : 'DELETE'
                },
                getAll : {
                    method : 'GET',
                    isArray : true
                },
                getOne : {
                    method : 'GET'
                }
            });
    });
