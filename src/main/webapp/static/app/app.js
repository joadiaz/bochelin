angular.module('bochelin', [
  'ngResource',
  'ui.router',
  'angular-loading-bar',
  'uiGmapgoogle-maps',
  'ui.bootstrap',
  'ngAnimate',
  'angularFileUpload',
  'ngCookies',
  'dialogs.main',
  'smart-table',
  'angular-carousel',
  'ngMaterial',
  'md.data.table',
  'chart.js'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('httpRequestInterceptor');
  })
  .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
  }])
  .config(['dialogsProvider','$translateProvider',function(dialogsProvider,$translateProvider) {
    dialogsProvider.useBackdrop('static');
    dialogsProvider.useEscClose(false);
    dialogsProvider.useCopy(false);
    dialogsProvider.setSize('sm');

    $translateProvider.translations('es', {
      DIALOGS_OK: "Bueno",
      DIALOGS_YES: "Si",
      DIALOGS_NO: "No",
      DIALOGS_CLOSE: "Cerrar"
    });

    $translateProvider.preferredLanguage('es');
  }])
  .constant('_', window._)
  .constant('PERMISSIONS',{
      private: 'private',
      public: 'public'
  })
  .constant('ROLES',{
    private: 'MANAGER',
    public: 'OWNER'
  })
  .constant('MESSAGES',{
    required: 'Campo requerido',
    tooLong: 'Muy largo!',
    tooShort: 'Muy corto!',
    invalidForm: 'Formulario Invalido',
    acceptDelete: 'Borrar',
    deniedDelete: 'No borrar'
  })
  .constant('SERVER',{
      address: '/api'
  })
  .run(function($rootScope, $state, PERMISSIONS, AuthService, Session, cartService) {
    console.log("Bochelin is running.");
    $rootScope.$on('$stateChangeStart', function(event, next){
      cartService.getFromCookie();
      var permissions = next.data.permissions;
      if (permissions == PERMISSIONS.private){
        if(!Session.isAuthenticated()){
          event.preventDefault();
          $state.go('login')
        }
      }
    });
  });
