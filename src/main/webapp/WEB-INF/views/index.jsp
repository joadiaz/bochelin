<html ng-app="bochelin">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <base href="/">

        <title>Bochelin</title>

        <!-- injector:css -->
        <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="/bower_components/angular-loading-bar/build/loading-bar.css">
        <link rel="stylesheet" href="/bower_components/angular-dialog-service/dist/dialogs.min.css">
        <link rel="stylesheet" href="/bower_components/components-font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="/bower_components/angular-carousel/dist/angular-carousel.css">
        <link rel="stylesheet" href="/bower_components/angular-material/angular-material.css">
        <link rel="stylesheet" href="/bower_components/angular-material-data-table/dist/md-data-table.css">
        <link rel="stylesheet" href="/app/app.css">
        <!-- endinjector -->


        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>


    </head>
    <body >
        <div ui-view="header"></div>
        <div ui-view></div>
        <div ui-view="footer"></div>

        <script src="/node_modules/chart.js/dist/Chart.min.js"></script>
        <!-- injector:js -->
        <script src="/bower_components/jquery/dist/jquery.js"></script>
        <script src="/bower_components/angular/angular.js"></script>
        <script src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>
        <script src="/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
        <script src="/bower_components/angular-resource/angular-resource.js"></script>
        <script src="/bower_components/angular-loading-bar/build/loading-bar.js"></script>
        <script src="/bower_components/lodash/lodash.js"></script>
        <script src="/bower_components/angular-google-maps/dist/angular-google-maps.js"></script>
        <script src="/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
        <script src="/bower_components/angular-animate/angular-animate.js"></script>
        <script src="/bower_components/angular-file-upload/dist/angular-file-upload.min.js"></script>
        <script src="/bower_components/angular-cookies/angular-cookies.js"></script>
        <script src="/bower_components/angular-sanitize/angular-sanitize.js"></script>
        <script src="/bower_components/angular-translate/angular-translate.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs.min.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs-default-translations.min.js"></script>
        <script src="/bower_components/angular-dialog-service/dist/dialogs-default-translations.js"></script>
        <script src="/bower_components/angular-smart-table/dist/smart-table.js"></script>
        <script src="/bower_components/angular-touch/angular-touch.js"></script>
        <script src="/bower_components/angular-carousel/dist/angular-carousel.js"></script>
        <script src="/bower_components/angular-aria/angular-aria.js"></script>
        <script src="/bower_components/angular-messages/angular-messages.js"></script>
        <script src="/bower_components/angular-material/angular-material.js"></script>
        <script src="/bower_components/angular-material-data-table/dist/md-data-table.js"></script>
        <script src="/app/app.js"></script>
        <script src="/app/admin/categories/categories.controller.js"></script>
        <script src="/app/admin/categories/categories.service.js"></script>
        <script src="/app/admin/categories/edit/categories.edit.js"></script>
        <script src="/app/admin/flavours/edit/flavours.edit.js"></script>
        <script src="/app/admin/flavours/flavours.controller.js"></script>
        <script src="/app/admin/flavours/flavours.service.js"></script>
        <script src="/app/admin/home/home.controller.js"></script>
        <script src="/app/admin/presentations/edit/presentations.edit.js"></script>
        <script src="/app/admin/presentations/presentations.controller.js"></script>
        <script src="/app/admin/presentations/presentations.service.js"></script>
        <script src="/app/admin/products/edit/products.edit.js"></script>
        <script src="/app/admin/products/products.controller.js"></script>
        <script src="/app/admin/products/products.service.js"></script>
        <script src="/app/admin/reports/reports.controller.js"></script>
        <script src="/app/admin/reports/reports.service.js"></script>
        <script src="/app/checkout/checkout.controller.js"></script>
        <script src="/app/checkout/checkout.service.js"></script>
        <script src="/app/common/auth.service.js"></script>
        <script src="/app/common/headerauth.factory.js"></script>
        <script src="/app/common/messages.service.js"></script>
        <script src="/app/common/session.service.js"></script>
        <script src="/app/common/upload.factory.js"></script>
        <script src="/app/home/home.controller.bk.js"></script>
        <script src="/app/home/home.controller.js"></script>
        <script src="/app/login/login.controller.js"></script>
        <script src="/app/routes.js"></script>
        <script src="/app/shop/cart.service.js"></script>
        <script src="/app/shop/item/shop.item.controller.js"></script>
        <script src="/app/shop/shop.controller.js"></script>
        <script src="/app/shop/shop.service.js"></script>
        <script src="/components/admin-menu/admin.menu.controller.js"></script>
        <script src="/components/backend-slider/slider.controller.js"></script>
        <script src="/components/file-upload/fileupload.directive.js"></script>
        <script src="/components/footer/footer.controller.js"></script>
        <script src="/components/header-admin/header.admin.controller.js"></script>
        <script src="/components/header/header.controller.js"></script>
        <script src="/components/search-flavour/searchflavour.directive.js"></script>
        <script src="/components/slider/slider.controller.js"></script>
        <!-- endinjector -->
        <script src="/node_modules/angular-chart.js/dist/angular-chart.min.js"></script>
    </body>
</html>